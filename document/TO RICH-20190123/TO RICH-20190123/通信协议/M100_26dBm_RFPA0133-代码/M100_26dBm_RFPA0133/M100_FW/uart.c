#include "uart.h"
#include "util.h"

#define  SYSCLKFREQ_H   (260000U) // 26M = 26000000 = 260000 * 100, unit is 100, because baudrate is always integral multiple of 100
#define  UARTCLKFREQ_H  (8125U)  // (SYSCLKFREQ_H / 32) = 8125
/*********************************************
Abstract   : Serial Port 0 initialize
Date:      : 2-24-2011
Description: Baudrate = 9600, using Timer 0
*********************************************/

//void Uart0_ini()
//{
//	SCON0 |= 0xD0;   //Mode 1: Async,8 bits, 0,1 ,receive enable
//	TMOD  |= 0x20;	 //Timer 1 mode 2 8-bit aut-reload
//	TH1    = 0xF8;  //(256-26M/32/9600/12)=0xF9,SMOD1=0
//	TL1    = 0xF8;
//	TR1    = 1;
//	ES0    = 1;
//}

/******************************************************************
* Abstract��SendByte
* description: Send a byte by Uart
* parameter: dat, data to send
* return: none
******************************************************************/
void SendByte(uchar dat)
{
	SBUF = dat;
	while(!TI);  //wait finish
	TI = 0;	   //clear sent flag
}

void SetUartBaudrate(uint baudrate)
{
	uint preloadValue = (65535U - (UARTCLKFREQ_H / baudrate)) + 1;
	RCAP2L = LSB(preloadValue);
	RCAP2H = MSB(preloadValue);
}

