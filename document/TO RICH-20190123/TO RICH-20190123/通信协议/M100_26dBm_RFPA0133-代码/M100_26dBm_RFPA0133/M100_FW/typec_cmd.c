#include "typec_cmd.h"
#include "uart.h"
#include "util.h"
#include "timer.h"
#include "config.h"
#include "fhss_lbt.h"

uchar xdata *p_ExMem;

uchar curPaPowerIndex;
uchar xdata mixerGain;
uchar xdata IFAmpGain;
uint xdata signalThreshold;

extern uint idata loopCnt;
uchar xdata selCombParam;
uchar xdata selPtr3;
uchar xdata selPtr2;
uchar xdata selPtr1;
uchar xdata selPtr0;
uchar xdata selMaskLen;
uchar xdata selTruncate;
uchar xdata selMask[32] _at_ SELECT_MASK_START_ADDR;
#ifdef _FEATURE_TRUNCATED_
uchar xdata truncatedEpc[64] _at_ TRUNCATED_EPC_START_ADDR;
#endif

uint code rssiTable[]={
	0x0022, 0x0026, 0x002B, 0x0030, 0x0036, 0x003D, 0x0044, 0x004C, 0x0056,
	0x0060, 0x006C, 0x0079, 0x0088, 0x0099, 0x00AC, 0x00C1, 0x00D8, 0x00F3,
	0x0110, 0x0132, 0x0157, 0x0181, 0x01B0, 0x01E5, 0x0220, 0x0262, 0x02AD,
	0x0300, 0x035E, 0x03C7, 0x043E, 0x04C2, 0x0557, 0x05FE, 0x06B9, 0x078B,
	0x0876, 0x097F, 0x0AA7, 0x0BF4, 0x0D6A, 0x0F0D, 0x10E3, 0x12F3, 0x1543,
	0x17DB, 0x1AC4, 0x1E08, 0x21B2, 0x25CF, 0x2A6C, 0x2F99, 0x3568, 0x3BEC,
	0x433C, 0x4B70, 0x54A5, 0x5EF9, 0x6A8F, 0x7790, 0x8627, 0x9685, 0xA8E3,
	0xBD7E
};
uint code * rssiTable0dBm = rssiTable + 46; // from -60 dBm

uchar code mixerGainTable[] = {
	0, 3, 6, 9, 12,	15, 16
};

code uchar IFAmpGainTable[] = {
	12, 18,	21,	24,	27,	30,	36,	40
};

idata uchar inventoryMode;

void InitParam(void)
{
	uchar i;
	loopCnt = 0;
	selCombParam = TARGET_S0 | ACTION_000 | MEM_BANK_EPC;
	selPtr3 = 0x00;
	selPtr2 = 0x00;
	selPtr1 = 0x00;
	selPtr0 = EPC_START_BIT_POINTER;
	selMaskLen = 0;
	selTruncate = 0x00;
	for(i = 0;i <= 31;i++)
	{
		selMask[i] = 0x00;
	}
	inventoryMode = 0xFF; // user doesn't set a mode
	fhss_status = FHSS_DONE;
//	time_tick = 0;  // ervery 0.1ms increase
	last_status_elapsed = 0;
	curRegion = REGION_CHN2;
	curFreqIndex = 7;
	freq_list.chCnt = 0;
	curPaPowerIndex = 11;
	mixerGain = 2;
	IFAmpGain = 6;
	signalThreshold = 0x00B0;
	SetModemPara(mixerGain, IFAmpGain, signalThreshold);
}

void GetModemPara()
{
	resDat.byteLen = 4;
	resDat.dat[0] = mixerGain;
	resDat.dat[1] = IFAmpGain;
	resDat.dat[2] = MSB(signalThreshold);
	resDat.dat[3] = LSB(signalThreshold);
	send_response(CMD_READ_MODEM_PARA, &resDat);
}

void SetEnvMode(uchar mode)
{
	if (mode == MODE_DENSE_READER)
	{
		mixerGain = 1; // 3dB
		IFAmpGain = 2; // 21dB
		signalThreshold = 0x0042;
		SetModemPara(mixerGain, IFAmpGain, signalThreshold);
	}
	else
	{
		mixerGain = 2; // 6dB
		IFAmpGain = 6; // 36dB
		signalThreshold = 0x00A0;
		SetModemPara(mixerGain, IFAmpGain, signalThreshold);
	}
	send_success(CMD_SET_ENV_MODE, 0);
}

void SetFhss(uchar enable)
{
	if (enable == FHSS_ON)
	{
		fhss_status = CW_IDLE;
		ET0 = 1;
		TR0 = 1;		
	}
	else
	{
		ET0 = 0;
		TR0 = 0;
		fhss_status = FHSS_DONE;
	}
}

BOOL isFhssOn()
{
	if (ET0 == 1 && TR0 == 1)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

void ManualSetPA(void)
{
	if ((PD_REG0&0x10)==0x10)	// PA is closed
	{
		OpenPA();
	}
	else
	{
		ClosePA();
	}
}

code unsigned char power_apc_i_config_table[12] = {
	/* 15dBm ~ 18dBm */
	0x30, 0x20, 0x28, 0x20,
	/* 19dBm ~ 24dBm */
	0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
	/* 25dBm ~ 26dBm */
	0x30, 0x30
};

code unsigned char power_internal_PAPWR_table[12] = {
	/* 15dBm ~ 18dBm */
	0x80, 0x81, 0x82, 0x83,
	/* 19dBm ~ 24dBm */
	0x83, 0x84, 0x85, 0x86, 0x87, 0x88,
	/* 25dBm ~ 26dBm */
	0x89, 0x8C
};

code unsigned char power_pa_bias_config_table[12] = {
	0xC4, 0xC4, 0xC4, 0xC4,
	0xC4, 0xC4, 0xC4, 0xC4, 0xC4, 0xC4,
	0xC4, 0xC4
};

void OpenPA()
{
	uchar papwrFinal = power_internal_PAPWR_table[curPaPowerIndex];
	uchar i;
	PACFG = power_pa_bias_config_table[curPaPowerIndex];
	PD_REG1		= 0x80;
	PD_REG0		= 0x80;
	APC_R_CFG	= 0x03;											//sfr 0xDD
	APC_I_CFG = power_apc_i_config_table[curPaPowerIndex];		//sfr 0xDC
	P1CON		&= ~(BIT2);
	P1         |= (BIT2);
	for (i = 0x80; i <= papwrFinal; i++)
	{
		PAPWR = i;
		delayus(3);
	}
//	PAPWR = power_internal_PAPWR_table[curPaPowerIndex];
}
void ClosePA()
{
	uchar papwrInital = power_internal_PAPWR_table[curPaPowerIndex];
	uchar i;
//	PAPWR		= 0x80;		//sfr 0xF9
	for (i = papwrInital; i >= 0x80; i--)
	{
		PAPWR = i;
		delayus(3);
	}
	APC_R_CFG	= 0x00;		//sfr 0xDD
	APC_I_CFG  	= 0x00;		//sfr 0xDC
	P1CON		|= (BIT2);
	P1   		&= ~(BIT2);
	PD_REG1		= 0xFF; 	//sfr 0xA0;
	PD_REG0		= 0xFE; 	//sfr 0x90;
}

code signed int tx_power_dBm_table[12]={
	1500, 1600, 1700, 1800, 1900, 2000,
	2100, 2200, 2300, 2400, 2500, 2600
};

void SetPower(signed int power)
{
	uchar i;
	for (i = 0; i < sizeof(tx_power_dBm_table)/sizeof(tx_power_dBm_table[0]); i++)
	{
		if (power <= tx_power_dBm_table[i] + 50)
		{
			break;
		}
	}
	if (i >= sizeof(tx_power_dBm_table)/sizeof(tx_power_dBm_table[0]))
	{
		i = sizeof(tx_power_dBm_table)/sizeof(tx_power_dBm_table[0]) - 1;
	}
	curPaPowerIndex = i;
	PAPWR = power_internal_PAPWR_table[curPaPowerIndex];
	PACFG = power_pa_bias_config_table[curPaPowerIndex];
	APC_I_CFG = power_apc_i_config_table[curPaPowerIndex];
}

void GetPower()
{
	int Power_dBm = tx_power_dBm_table[curPaPowerIndex];
	resDat.byteLen = 2;
	resDat.dat[0] = MSB(Power_dBm);
	resDat.dat[1] = LSB(Power_dBm);
	send_response(CMD_GET_POWER, &resDat);
}

uchar GetChannelNum()
{
	switch (curRegion)
	{
		case REGION_CHN2:
		case REGION_CHN1:
			return 20;
			break;
		case REGION_US:
			return 52;
			break;
		case REGION_KOREA:
			return 32;
			break;
		case REGION_EUR:
			return 15;
			break;
		default:
			return 20;
			break;
	}
}

void ScanJammer()
{
	uchar i;
	idata uchar channelNum = GetChannelNum();
	idata BOOL fhssStatus = isFhssOn();
    idata oldFreqIndex = curFreqIndex;
	if (fhssStatus)  // turn off FHSS to avoid the disturbance of fhss
	{
		SetFhss(FHSS_OFF);
	}
	OpenPA();
	delayus(2000);
	resDat.byteLen = 2 + channelNum;
	resDat.dat[0] = 0;
	resDat.dat[1] = channelNum - 1;
	for (i = 0; i < channelNum; i++)
	{
		SetFreq(i, curRegion, FALSE);
		delayus(1000);
		resDat.dat[2 + i] = GetJammer();
	}
	SetFreq(oldFreqIndex, curRegion, FALSE);
	ClosePA();
	if (fhssStatus)
	{
		SetFhss(FHSS_ON);
	}
}

void ScanRssi()
{
	idata uchar channelNum = GetChannelNum();
	idata BOOL fhssStatus = isFhssOn();
	xdata oldFreqIndex = curFreqIndex;
	xdata uchar oldIFAMPCFG = IFAMPCFG;
	xdata uchar oldMXCFG = MXCFG;
	char i;

	if (fhssStatus)  // turn off FHSS to avoid the disturbance of fhss
	{
		SetFhss(FHSS_OFF);
	}
    
	resDat.byteLen = 2 + channelNum;
	resDat.dat[0] = 0;
	resDat.dat[1] = channelNum - 1;
    PD_REG1		= 0x80;
	PD_REG0		= 0x90;
	delayus(1000);
	for (i = 0; i < channelNum; i++)
	{
		SetFreq(i, curRegion, TRUE);
		delayus(1000);
		resDat.dat[2 + i] = GetRssi();
		delayus(3000);
	}
	SetFreq(oldFreqIndex, curRegion, FALSE);
	IFAMPCFG = oldIFAMPCFG;
	MXCFG = oldMXCFG;
	if (fhssStatus)
	{
		SetFhss(FHSS_ON);
	}
	PD_REG1		= 0xFF; //0xA0;
	PD_REG0		= 0xFE; //0x90; 
	delayus(1000);
	send_response(CMD_SCAN_RSSI, &resDat);
}

#if 0
void AutoInsertChannel(uchar chNum, BOOL needResponse)
{
	uchar i, j;
	xdata uchar freqChAdjust[52];
	uchar maxjammerPosition = 0;
	curPaPowerIndex = 0;
	InsertFhssChannel(freqChAdjust, 0); // clear all frequency points in the FHSS Channel list
	ScanJammer();
	for (i = 0; i < chNum; i++)
	{
		freqChAdjust[i] = i;
	}
	for (i = 0; i < chNum; i++)
	{
		if ((char)resDat.dat[2 + freqChAdjust[maxjammerPosition]] < (char)resDat.dat[2 + freqChAdjust[i]])
		{
			maxjammerPosition = i;
		}
	}
	for (i = chNum; i < GetChannelNum(); i++)
	{
		if ((char)resDat.dat[2 + freqChAdjust[maxjammerPosition]] > (char)resDat.dat[2 + i])
		{
			freqChAdjust[maxjammerPosition] = i;
		}
		for (j = 0; j < chNum; j++)
		{
			if ((char)resDat.dat[2 + freqChAdjust[maxjammerPosition]] < (char)resDat.dat[2 + freqChAdjust[j]])
			{
				maxjammerPosition = j;
			}
		}
	}
//	for (j = 0; j < sizeof(freqChAdjust); j++)
//	{
//		send_debug_byte(freqChAdjust[j]);
//	}
	if (needResponse)
	{
		resDat.byteLen = chNum + 1;
		resDat.dat[0] = chNum;
		for (i = 0; i < chNum; i++)
		{
			resDat.dat[i+1] = freqChAdjust[i];
		}
		send_response(CMD_AUTO_ADJUST_CH, &resDat);
	}
	InsertFhssChannel(freqChAdjust, chNum);
	SetFhss(FHSS_ON);
	curPaPowerIndex = sizeof(tx_power_dBm_table)/sizeof(tx_power_dBm_table[0]) - 1;
}
#endif

void BuildEbv(uint source, ebv_type *dst)
{
	dst->byte_length = 0;

	if(source < (1 << 7) )
	{
		dst->byte_length = 1;
		dst->ebv[0] = (uchar) source;
	}
	else if(source < (1<< 14))
	{
		dst->byte_length = 2;
		dst->ebv[0] = source >> 7;
		dst->ebv[0] += 0x80;
		dst->ebv[1] = source & 0x7F;
	}
	else
	{
		dst->byte_length = 3;
		dst->ebv[0] = source >> 14;
		dst->ebv[0] += 0x80;
		dst->ebv[1] = (0x7F) & (source >> 7);
		dst->ebv[1] += 0x80;
		dst->ebv[2] = source & 0x7F;
	}
}

void GetInvResult(void)
{
	int xdata i;
	uchar xdata PCEPCLength = 0;
	uchar xdata checksum = 0;
	uchar xdata j  = 0;
	uchar xdata datalen = 0;
	uint xdata RssiI = 0;
	uint xdata RssiQ = 0;
	uint xdata Rssi = 0;
	char xdata rssidBm = 0;
	p_ExMem = 0;
	if(EPCNUM > 0) 
	{
		for(j = 0;j < EPCNUM; j++)
		{
			checksum = 0;
			SendByte(FRAME_HEAD);		//frame begin
			SendByte(FRAME_INFO);
			SendByte(CMD_SINGLE_ID);
			SendByte(0x00);
			if (IVRARGU & INVENTORY_MASK_SAVE_RSSI) // with RSSI
			{
				PCEPCLength = ((((*(p_ExMem+4)) >> 3) + 1) << 1);
				datalen = PCEPCLength + 3; // RSSI/1(in dBm, only need one char) + CRC/2
				RssiI = ((*p_ExMem) << 8) | *(p_ExMem+1);
				RssiQ = (*(p_ExMem+2) << 8) | *(p_ExMem+3);
				p_ExMem += 4;
				// calculate the square root
				if (RssiI > RssiQ)
				{
					Rssi = RssiI + RssiQ / 8 * 3;
				}
				else
				{
					Rssi = RssiQ + RssiI / 8 * 3;
				}
				for (i = 0; i < (sizeof(rssiTable)/sizeof(rssiTable[0])); i++)
				{
					if (Rssi <= rssiTable[i])
					{
						rssidBm = i - 46;
						break;
					}
				}
				rssidBm -= mixerGainTable[mixerGain];
				rssidBm -= IFAmpGainTable[IFAmpGain];
				checksum += rssidBm;
				SendByte(datalen);
				SendByte(rssidBm);
			}
			else
			{
				PCEPCLength = ((*(p_ExMem) >> 3) + 1) << 1;
				datalen = PCEPCLength + 2; // CRC/2
				SendByte(datalen);  
			}
			checksum += (FRAME_INFO + CMD_SINGLE_ID) + datalen;
			for(i = 0;i < (PCEPCLength + 2); i++)  //exclude TYPE/1 + CODE/1 + FRAMELEN/2 + CHECKSUM/1 
			{
				checksum += *p_ExMem;
				SendByte(*p_ExMem);
				p_ExMem++; 
			}
			SendByte(checksum);
			SendByte(FRAME_END);
		}
	}
#ifdef _FEATURE_TRUNCATED_
	else if ((EPCNUMERR > 0) && (IVRARGU & INVENTORY_MASK_TRUNCATED))
	{
		uchar xdata *pTrucEpc;
		uchar idata	selMaskByteLen 	= selMaskLen >> 3;
		uchar idata selMaskResBitLen = selMaskLen - (selMaskByteLen << 3);
		uchar idata trucResLen;
		PCEPCLength = ((selMask[0] >> 3) + 1) << 1;
		for(j = 0;j < EPCNUMERR; j++)
		{
			checksum = 0;
			pTrucEpc = TRUNCATED_EPC_START_ADDR;
			if (IVRARGU & INVENTORY_MASK_SAVE_RSSI) // with RSSI
			{
				RssiI = ((*p_ExMem) << 8) | *(p_ExMem+1);
				RssiQ = (*(p_ExMem+2) << 8) | *(p_ExMem+3);
				p_ExMem += 4;
				if (RssiI > RssiQ)
				{
					Rssi = RssiI + RssiQ / 8 * 3;
				}
				else
				{
					Rssi = RssiQ + RssiI / 8 * 3;
				}
				datalen =  PCEPCLength + 4;
			}
			else
			{
				datalen =  PCEPCLength + 2;
			}

			for (i = 0; i < selMaskByteLen; i++)
			{
				*pTrucEpc = selMask[i];
				pTrucEpc++;					
			}

			if (selMaskResBitLen != 0)
			{
				trucResLen = (TAGRSPLEN0 >> 3) + 2;
				*pTrucEpc = selMask[selMaskByteLen] & (0xFF << (8 - selMaskResBitLen));
				if (selMaskResBitLen < 5)	
				{
					*pTrucEpc |= ((*p_ExMem) << 5) >> selMaskResBitLen; 
					p_ExMem++;
					*pTrucEpc |= (*p_ExMem) >> (8 - (5 - selMaskResBitLen));
					pTrucEpc++;
					for (i = 0; i < trucResLen - 1; i++)
					{
						*pTrucEpc = ((*p_ExMem) << (5 - selMaskResBitLen)) | ((*(p_ExMem + 1)) >> (8 - (5 - selMaskResBitLen)));
						pTrucEpc++;
						p_ExMem++;
					}
				}
				else
				{
					*pTrucEpc |= ((*p_ExMem) << 5) >> selMaskResBitLen;
					pTrucEpc++;
					for (i = 0; i < trucResLen; i++)
					{
						*pTrucEpc = ((*p_ExMem) << (8 - (selMaskResBitLen - 5))) | ((*(p_ExMem + 1)) >>  (selMaskResBitLen - 5));
						pTrucEpc++;
						p_ExMem++;
					}
				}
			}
			else
			{
				trucResLen = (TAGRSPLEN0 >> 3) + 2;
				for (i = 0; i < trucResLen; i++)
				{
					*pTrucEpc = ((*p_ExMem) << 5) | ((*(p_ExMem + 1)) >> 3);
					p_ExMem++;
					pTrucEpc++;
				}	
			}
			pTrucEpc = TRUNCATED_EPC_START_ADDR;
			if (CRCRight(pTrucEpc,PCEPCLength + 2))
			{
				SendByte(FRAME_HEAD);		//frame begin
				SendByte(FRAME_INFO);
				SendByte(CMD_SINGLE_ID);
				SendByte(0x00);
				SendByte(datalen);
				checksum += (FRAME_INFO + CMD_SINGLE_ID) + datalen;
				if (IVRARGU & INVENTORY_MASK_SAVE_RSSI) // with RSSI
				{
					SendByte(MSB(Rssi));
					SendByte(LSB(Rssi));
					checksum += MSB(Rssi) + LSB(Rssi);
				}
				pTrucEpc = TRUNCATED_EPC_START_ADDR;
				for (i = 0; i < PCEPCLength + 2; i++)
				{
					SendByte(*pTrucEpc);
					checksum += *pTrucEpc;
					pTrucEpc++;	
				}
				SendByte(checksum);
				SendByte(FRAME_END);
			}
			p_ExMem++;
		}
	}
#endif
	else if (EPCNUM == 0x00)
	{
		send_fail(FAIL_INVENTORY_TAG_TIMEOUT, 0);
	}
}

void InventoryOnce(void)
{
#ifdef _FEATURE_TRUNCATED_
	uchar trucResLen = 0;
#endif
	if (CheckFhssStatus() != FHSS_WORK_SLOT_AVAILABLE)
	{
		send_fail(FAIL_FHSS_FAIL, 0);
		return;
	}
	fhss_status = OPT_WORKING;
	OpenPA();
	delayus(660);
	EX0 = 0;
#ifdef RSSI_STORE
	IVRARGU = INVENTORY_MASK_NO_NAK | INVENTORY_MASK_SAVE_RSSI;
#else
	IVRARGU = INVENTORY_MASK_NO_NAK;
#endif
	if (inventoryMode == INVENTORY_MODE0)
	{
		Select();
#ifdef _FEATURE_TRUNCATED_
		if (selTruncate & 0x80)
		{
			IVRARGU |= (INVENTORY_MASK_NO_NAK | INVENTORY_MASK_TRUNCATED); //INVENTORY_PARAM_TRUNCATED;	No NAK
			trucResLen = ((((selMask[0] >> 3) + 1) << 1) << 3) - selMaskLen;
			if (trucResLen % 8 == 0)
			{
				TAGRSPLEN0 =  trucResLen;
                CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set respones length high 4 bits to 0
			} 
			else
			{
				TAGRSPLEN0 =  ((trucResLen >> 3) + 1) << 3;	   // reset EPC value , add 8 to avoid reset bits being abandoned
                CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set respones length high 4 bits to 0
			}
		}
#endif
		delayus(200);
	}
	STARTOPT(INVENTORY_OPT);
	while(!MACRODONE);
	ClosePA();
	MACROEN = 0;
	GetInvResult();
	fhss_status = FHSS_DONE;
}

void StopMulti(void)
{
	loopCnt = 0;

	send_success(CMD_STOP_MULTI, 0);
}

void SetQueryParam(uchar MSB, uchar LSB)
{
	QUERYARGU0 = (MSB & 0x0F) | 0x10; // DR, M, Trext doesn't support change, Trext default 1
	QUERYARGU1 = LSB >> 3;
	send_success(CMD_SET_QUERY_PARA, 0);
}

void GetQueryParam(void)
{
	resDat.byteLen = 2;
	resDat.dat[0] = QUERYARGU0;
	resDat.dat[1] = QUERYARGU1 << 3;
	send_response(CMD_GET_QUERY_PARA, &resDat);
}

void GetSelParam(void)
{
	uchar selMaskByteLen;
	uchar selMaskResBitLen;
	uchar i;

	selMaskByteLen 	= selMaskLen >> 3;
	selMaskResBitLen = selMaskLen - (selMaskByteLen << 3);

	resDat.dat[0] = selCombParam;
	resDat.dat[1] = selPtr3;
	resDat.dat[2] = selPtr2;
	resDat.dat[3] = selPtr1;
	resDat.dat[4] = selPtr0;
	resDat.dat[5] = selMaskLen;
	resDat.dat[6] = selTruncate;

	for(i=0; i<selMaskByteLen; i++)
	{
		resDat.dat[7 + i] = selMask[i];
	}

	resDat.byteLen = 7 + selMaskByteLen;
	if(selMaskResBitLen != 0x00)
	{
		resDat.dat[7 + selMaskByteLen] = selMask[selMaskByteLen];
		resDat.byteLen++;
	}

	send_response(CMD_GET_SELECT_PARA, &resDat);
}

void SetSelParam(uchar * rxBuf)
{
	uchar selMaskByteLen;
	uchar selMaskResBitLen;
	uchar i;

	if (inventoryMode != INVENTORY_MODE0)
	{
		inventoryMode = INVENTORY_MODE2;
	}

	selCombParam 	= rxBuf[5];
	selPtr3			  = rxBuf[6];
	selPtr2			  = rxBuf[7];
	selPtr1			  = rxBuf[8];
	selPtr0			  = rxBuf[9];
	selMaskLen		= rxBuf[10];
	selTruncate		= rxBuf[11];

	selMaskByteLen 	= selMaskLen >> 3;
	selMaskResBitLen = selMaskLen - (selMaskByteLen<<3);

	for (i=0; i<selMaskByteLen; i++)
	{
		selMask[i] = rxBuf[12+i];
	}
	if(selMaskResBitLen != 0x00)
	{
	 	selMask[selMaskByteLen] = rxBuf[12+selMaskByteLen] & (0xFF << (8 - selMaskResBitLen));
	}

	send_success(CMD_SET_SELECT_PARA, 0);
}

void SetInventoryMode(uchar mode)
{
	inventoryMode = mode;
	send_success(CMD_SET_INV_MODE, 0);
}

//Select All tag to Inventory Flag A or ~SL Flag
void SelectAllToAorInvSL(BOOL isToA)
{
	uchar tempselCombParam;
	CMDLEN  = 21 + 8;         //4+3+3+2+1 * 8+8+0+1;
	CMDARGU = CMD_IS_CRC16 | CMD_HAS_CRC | CMD_NO_RESPONSE | PIE_FRAME_SYNC;
	if (isToA)
	{
		tempselCombParam = (selCombParam & (~ACTION)) | ACTION_001; // action 001, set to a when matching, do noting when Non-match
	}
	else
	{
		tempselCombParam = (((selCombParam & (~TARGET)) | TARGET_SL) & (~ACTION)) | ACTION_101;
	}
	p_ExMem = 0;
	*p_ExMem = TYPEC_SEL_CMD | (tempselCombParam >> 4);
	p_ExMem++;
	*p_ExMem = (tempselCombParam << 4) | (0 >> 4); // point 0
	p_ExMem++;
	*p_ExMem = (0 << 4) | (0 >> 4); // maskLength 0, mask 0
	p_ExMem++;
	*p_ExMem = (0 << 4) | (0 >> 4); // no truncated
	STARTOPT(USER_DEFINE_OPT);
	while(!MACRODONE);
	delayus(30);
	MACROEN = 0;
}

void Select(void)
{
	uchar maskLenInByte;
	uchar maskLenRestbits;
	uchar i = 0;
	char j = 0;
	uchar splitTime = selMaskLen > 0 ? (((selMaskLen - 1) / SINGLE_SELECT_BIT_LEN_MAX) + 1) : 1;
	uchar tempMaskLen;
	uchar tempSelMaskByteCnt = 0;
	uchar tempselCombParam;
	xdata uchar action;
	xdata uchar target;
	xdata ebv_type ptr;
	xdata uint maskPtr;
	maskPtr = (selPtr1 << 8) | selPtr0;
	EX0 = 0;

	target = selCombParam & TARGET;
	action = selCombParam & ACTION; // 0001 1100

	if (splitTime > 1) // mask length > SINGLE_SELECT_BIT_LEN_MAX(80 bits)
	{
		//Select All tag to A & ~SL
		if (target > TARGET_S0)
		{
			if (target == TARGET_SL)
			{
				SelectAllToAorInvSL(FALSE);
			}
			else
			{
				SelectAllToAorInvSL(TRUE);
			}
		}
	}

	for (j = 0; j < splitTime; j++)
	{
		if (j < splitTime - 1)
		{
			tempMaskLen = SINGLE_SELECT_BIT_LEN_MAX;
		}
		else if (selMaskLen == 0) // sometimes, user use maskLen = 0 to select all tags!
		{
			tempMaskLen = 0;
		}
		else
		{
			tempMaskLen = (selMaskLen % SINGLE_SELECT_BIT_LEN_MAX == 0) ? SINGLE_SELECT_BIT_LEN_MAX : (selMaskLen % SINGLE_SELECT_BIT_LEN_MAX);
		}
		if (j > 0) //not first time Select
		{
			/** because the max tx length is 104 bits, select command with too long mask length will not transmit
			 *  completely in a single tx. So we separate the select command into several short select commands.
			 *  First select command use the user defined selCombParam(Target & Action), and the latter select
			 *  commands should transfer Action by Select Target(S0~S3 or SL).
			 *  	   Action	Match	Non-Match	new Action for Target S0~S3	      new Action for Target SL
			 *  		000		A,SL	  B,~SL				  010								 010
			 *  		001		A,SL	  A,~SL				  001								 010
			 *  		010		A,~SL	  B,~SL				  010								 010
			 *  		011		B,~SL	  A,~SL				  110								 010
			 *  		100		B,~SL	  A,SL				  110								 110
			 *  		101		B,~SL	  A,~SL				  110								 101
			 *  		110		A,~SL	  A,SL				  110								 110
			 *  		111		A,~SL	  B,SL				  010								 110
			 */
			if (target != TARGET_SL)
			{
				if (action >= ACTION_011 && action <= ACTION_110)
				{
					action = ACTION_110;
				}
				else if (action == ACTION_000 || action == ACTION_111)
				{
					action = ACTION_010;
				}
			}
			else
			{
				if (action <= ACTION_011)
				{
					action = ACTION_010;
				}
				else if (action != ACTION_101)
				{
					action = ACTION_110;
				}
			}
			tempselCombParam = (selCombParam & (~ACTION)) | action;
		}
		else
		{
			tempselCombParam = selCombParam;
		}

		i = tempSelMaskByteCnt;
		maskLenInByte = tempMaskLen >> 3;
		maskLenRestbits = tempMaskLen - (maskLenInByte << 3);
		BuildEbv(maskPtr, &ptr);
		//build Select command
		CMDLEN    = 21 + ptr.byte_length * 8 + tempMaskLen;	  //4+3+3+2+ptr.byte_length * 8+8+SelMask+1;
		CMDARGU = CMD_IS_CRC16 | CMD_HAS_CRC | CMD_NO_RESPONSE | PIE_FRAME_SYNC; //frame_sync, CRC-16, no response
		p_ExMem = 0;
		*p_ExMem = TYPEC_SEL_CMD | (tempselCombParam >> 4);
		p_ExMem++;
		*p_ExMem = (tempselCombParam << 4) | (ptr.ebv[0] >> 4);
		p_ExMem++;
		if (ptr.byte_length == 1)
		{
			*p_ExMem = (ptr.ebv[0] << 4) | (tempMaskLen >> 4);
		}
		else if(ptr.byte_length == 2)
		{
			*p_ExMem = (ptr.ebv[0] << 4) | (ptr.ebv[1] >> 4);
			p_ExMem++;
			*p_ExMem = (ptr.ebv[1] << 4) | (tempMaskLen >> 4);
		}
		p_ExMem++;
		*p_ExMem = (tempMaskLen << 4) | (selMask[i] >> 4);
		if(maskLenInByte == 0)
		{
			if(maskLenRestbits < 4)
			{
				*p_ExMem = (*p_ExMem & (0xFF << (4-maskLenRestbits))) | (selTruncate >> (4 + maskLenRestbits));
			}
			else
			{
				p_ExMem++;
				*p_ExMem = (selMask[i] << 4) | (selTruncate >> (maskLenRestbits - 4));
			}
		}
		else
		{
			if (maskLenRestbits == 0)
			{
				for(i = tempSelMaskByteCnt; i < (maskLenInByte+tempSelMaskByteCnt); i++)
				{
					p_ExMem++;
					*p_ExMem = (selMask[i] << 4) | (selMask[i+1] >> 4);
				}
				*p_ExMem = (*p_ExMem & 0xF0) | (selTruncate >> 4);
			}
			else
			{
				for(i = tempSelMaskByteCnt; i < (maskLenInByte+tempSelMaskByteCnt); i++)
				{
					p_ExMem++;
					*p_ExMem = (selMask[i] << 4) | (selMask[i+1] >> 4);
				}
				if(maskLenRestbits < 4)
				{
					//*p_ExMem = (selMask[tempSelMaskCnt+maskLenInByte] >> 4) | (selTruncate >> (4+maskLenRestbits));
					*p_ExMem = (*p_ExMem & (0xFF << (4-maskLenRestbits))) | (selTruncate >> (4+maskLenRestbits));
				}
				else
				{
					p_ExMem++;
					*p_ExMem = (selMask[tempSelMaskByteCnt + maskLenInByte] << 4) | (selTruncate >> (maskLenRestbits - 4));
				}
			}
		}
		STARTOPT(USER_DEFINE_OPT);
		while(!MACRODONE);
		delayus(30);
		MACROEN = 0;
		maskPtr += SINGLE_SELECT_BIT_LEN_MAX;
		tempSelMaskByteCnt += SINGLE_SELECT_BYTE_LEN_MAX;
	}
}

BOOL Req_RN(uchar * RN16H, uchar * RN16L)
{
	CMDLEN = 24;  // CMD/8 + handle/16
	CMDARGU = RES_DO_CRC16 | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE;// frame-sync, CRC-16 , response check CRC
	TAGRSPLEN0  = 16;
    CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set respones length high 4 bits to 0
	p_ExMem = 0;
	*p_ExMem = TYPEC_REQ_RN_CMD;
	p_ExMem++;
	*p_ExMem = HANDLEH;
	p_ExMem++;
	*p_ExMem = HANDLEL;

	STARTOPT(USER_DEFINE_OPT);
	while( !(MACRODONE) ); //wait Macro done
	//delay10u(5);
	if(MACRO_RXDONE && !MACRO_CRCERR)  // received data flag && CRC right
	{
		MACROEN = 0;  //disable Macro
		p_ExMem = 0;
		*RN16H = *p_ExMem;
		*RN16L = *(p_ExMem + 1);
		return TRUE;
	}
	MACROEN = 0;
	return FALSE;
}

void BuildAccessFrame(unsigned char *accessData)
{
	CMDLEN = 40;  // CMD/8 + PASSWORD/16 + handle/16
	CMDARGU = RES_DO_CRC16 | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE; // frame-sync, CRC-16 , response check CRC
	TAGRSPLEN0 = 16;
    CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set respones length high 4 bits to 0
	p_ExMem = 0;
	*p_ExMem = TYPEC_ACCESS_CMD;
	p_ExMem++;
	*p_ExMem = accessData[0];
	p_ExMem++;
	*p_ExMem = accessData[1];
	p_ExMem++;
	*p_ExMem = HANDLEH;
	p_ExMem++;
	*p_ExMem = HANDLEL;
}

BOOL Access(uint apH, uint apL)
{
	uchar accessData[2];
	uchar newRN16H;
	uchar newRN16L;
	//1. Req_RN
	if (Req_RN(&newRN16H, &newRN16L))
	{
		delayus(200);
		accessData[0] = newRN16H ^ MSB(apH);
		accessData[1] = newRN16L ^ LSB(apH);
		BuildAccessFrame(accessData);
		STARTOPT(USER_DEFINE_OPT);
		while(!MACRODONE);
		delayus(30);
		p_ExMem = 0;
		//check Access Step 1 response
		if((MACRO_RXDONE && !MACRO_CRCERR) && (*p_ExMem == HANDLEH) && (*(p_ExMem+1) == HANDLEL))
		{
			MACROEN = 0;  //disable Macro
			//Access step2
			//3.issue Req_RN
			//delay10u(2);
			if (Req_RN(&newRN16H, &newRN16L))
			{
				delayus(200);
				accessData[0] = newRN16H ^ MSB(apL);
				accessData[1] = newRN16L ^ LSB(apL);
				//4.issue ACCESS command
				BuildAccessFrame(accessData);
				STARTOPT(USER_DEFINE_OPT);
				while (!MACRODONE);
				delayus(20);
				p_ExMem = 0;
				//check Access Step 2 response
				if ((MACRO_RXDONE && !MACRO_CRCERR) && (*p_ExMem == HANDLEH) && (*(p_ExMem+1) == HANDLEL))
				{
					MACROEN = 0;
					return TRUE;
				}
			}
		}
	}
	MACROEN = 0;
	return FALSE;
}

void StorePcEpc()
{
	uchar PCEPCLength;
	uchar i;
	uchar xdata *p_StoredPcEpc;
	p_StoredPcEpc = PCEPC_STORED_START_ADDR;
	p_ExMem = 0;
	PCEPCLength = ((*(p_ExMem) >> 3) + 1) << 1;
	for (i = 0; i < PCEPCLength; i++)
	{
		*p_StoredPcEpc = *p_ExMem;
		p_ExMem++;
		p_StoredPcEpc++;
	}
}

BOOL InventoryAccess(uint accessPwdH, uint accessPwdL, fail_code_type default_fail_code)
{
	xdata uchar errCnt;
	xdata uchar errAccessCnt = 0;

	EX0= 0;  //close Macro Done interrupt
	if (CheckFhssStatus() != FHSS_WORK_SLOT_AVAILABLE)
	{
		send_fail(FAIL_FHSS_FAIL, 0);
		return FALSE;
	}
	fhss_status = OPT_WORKING;
	OpenPA();
	delayus(660);
INVENTORY_ACCESS_BEGIN:
	errCnt = 0;
	while (errCnt < 5)
	{
		if (inventoryMode != INVENTORY_MODE1)
		{
			Select();
		}
	    IVRARGU = INVENTORY_MASK_NO_NAK | INVENTORY_MASK_REQ_RN;
	    STARTOPT(INVENTORY_OPT);
	    while(!MACRODONE);
		MACROEN = 0;
		if (EPCNUM > 0)
		 {
			StorePcEpc();
			break;
		}
		errCnt++;
	}
	if (EPCNUM > 0)
	{
		if (!(accessPwdH == 0 && accessPwdL == 0))
		{
			if ((errAccessCnt >= 5))
			{
				ClosePA();
				fhss_status = FHSS_DONE;
				send_fail(FAIL_ACCESS_PWD_ERROR, PCEPC_LENGTH);
				return FALSE;
			}
			else if (errAccessCnt < 5)
			{
				if (!Access(accessPwdH, accessPwdL))
				{
					errAccessCnt++;
					goto INVENTORY_ACCESS_BEGIN;
				}
			}
			return TRUE;
		}
		// if Access Password is all zeros, don't need to send Access command
		return TRUE;
	}
	ClosePA();
	fhss_status = FHSS_DONE;
	send_fail(default_fail_code, 0);
	return FALSE;
}

uchar Read(uchar memBank, uint wordPtr, uchar wordCount)
{
	ebv_type ptr;
	uint READLEN = 16 + (wordCount << 4);
	BuildEbv(wordPtr, &ptr);
	CMDLEN = 34 + ptr.byte_length * 8;
	CMDARGU = RES_DO_CRC16 | RES_HAS_HEADER | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE;  // frame-sync, CRC-16 , response has header and check CRC
	TAGRSPLEN0 = LSB(READLEN);
    CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | (READLEN >> 8);
	p_ExMem = 0;
	*p_ExMem = TYPEC_READ_CMD;
	p_ExMem++;
	*p_ExMem = (memBank << 6) | (ptr.ebv[0] >> 2);
	p_ExMem++;
	if (ptr.byte_length == 1)
	{
		*p_ExMem = (ptr.ebv[0] << 6) | (wordCount >> 2);
		p_ExMem++;
	}
	else if (ptr.byte_length == 2)
	{
		*p_ExMem = (ptr.ebv[0] << 6) | (ptr.ebv[1] >> 2);
		p_ExMem++;
		*p_ExMem = (ptr.ebv[1] << 6) | (wordCount >> 2);
		p_ExMem++;
	}

	*p_ExMem = (wordCount << 6) | (HANDLEH >> 2);
	p_ExMem++;
	*p_ExMem = (HANDLEH << 6) | (HANDLEL >> 2);
	p_ExMem++;
	*p_ExMem = (HANDLEL << 6) & 0xC0;
	STARTOPT(USER_DEFINE_OPT);
	EX0 = 0;
	while( !(MACRODONE) );
	delayus(50);
	if (MACRO_RXDONE && !MACRO_CRCERR)
	{
		if (!MACRO_HEADERFLAG)
		{
			MACROEN = 0;
			return 0;
		}
		else if (MACRO_HEADERFLAG)
		{
			MACROEN = 0;
			p_ExMem = 0;
			return (FAIL_READ_ERROR_CODE_BASE | (*p_ExMem));
		}
	}
	MACROEN = 0;
	return FAIL_READ_MEMORY_NO_TAG;
}

uchar SendPcEpc(void)
{
	uchar i;
	uchar xdata * p_StoredPcEpc = PCEPC_STORED_START_ADDR;
	uchar PCEPCLength = ((*(p_StoredPcEpc) >> 3) + 1) << 1;
	uchar checksum = PCEPCLength;
	SendByte(PCEPCLength);
	for (i = 0; i < PCEPCLength; i++)
	{
		SendByte(*p_StoredPcEpc);
		checksum = checksum + (*p_StoredPcEpc);
		p_StoredPcEpc++;
	}
	return checksum;
}

void InventoryRead(uchar * msgBuf)
{
	xdata uint apH = (msgBuf[5] << 8) | msgBuf[6];
	xdata uint apL = (msgBuf[7] << 8) | msgBuf[8];
	xdata uchar memBank = msgBuf[9];
	xdata uint wordPtr = (msgBuf[10] << 8) | msgBuf[11];
	xdata uchar wordCount1 = msgBuf[12]; // the max read length is 4096 bits = 256 words, wordCount1 should always be 0
	xdata uchar wordCount0 = msgBuf[13];
	uint readLen = wordCount0 << 1;
	xdata uint resDataLen;
	uchar checksum = 0;
	xdata uchar errcode = 0;
	xdata uchar errCnt;
	uint i;
	if (InventoryAccess(apH, apL, FAIL_READ_MEMORY_NO_TAG))
	{
		errCnt = 0;
		while (errCnt < 5)
		{
			errcode = Read(memBank, wordPtr,wordCount0);
			if (0 == errcode)
			{
				break;
			}
			errCnt++;
		}
		ClosePA();
		fhss_status = FHSS_DONE;
		if (errCnt >= 5)
		{
			send_fail(errcode, PCEPC_LENGTH);
		}
		else
		{
			ClosePA();	
			p_ExMem = 0;
			SendByte(FRAME_HEAD);
			SendByte(FRAME_RES);
			SendByte(CMD_READ_DATA);
			resDataLen = readLen + PCEPC_LENGTH + 1;
			SendByte(MSB(resDataLen));
			SendByte(LSB(resDataLen));
			checksum = FRAME_RES + CMD_READ_DATA + MSB(resDataLen) + LSB(resDataLen);
			checksum += SendPcEpc();
			for(i = 0; i < readLen; i++)
			{
				checksum = checksum + (*p_ExMem);
				SendByte(*p_ExMem);
				p_ExMem++;
			}
			SendByte(checksum);
			SendByte(FRAME_END);
		}
	}
}

uchar Write(uchar memBank, uint wordPtr, uint writeData)
{
	uchar newRN16H;
	uchar newRN16L;
	uchar XORedData[2];
	ebv_type ptr;
	EX0= 0;  //close Macro Done interrupt
	delayus(5);
	//1. Req_RN
	if (!Req_RN(&newRN16H,&newRN16L))
	{
		return FAIL_WRITE_MEMORY_NO_TAG;
	}
	delayus(5);
	XORedData[0] = MSB(writeData) ^ newRN16H;
	XORedData[1] = LSB(writeData) ^ newRN16L;
	//2.Write
	//Build Write Command
	BuildEbv(wordPtr, &ptr);
	CMDLEN    = 42 + ptr.byte_length * 8;	  //CMD/8 + MemBank/2 + ptr.byte_length * 8 + Data/16 + handle/16
	CMDARGU = RES_DO_CRC16 | RES_HAS_HEADER | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE;// frame-sync, CRC-16 , response has header and check CRC-16
	TAGRSPLEN0 = 16;  // handle/16, header and CRC checker result will be saved to SFR
    CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set respones length high 4 bits to 0
	T1ARGU1 = LONG_T1_TIME_H;   // for 20ms delay
	p_ExMem = 0;
	*p_ExMem = TYPEC_WRITE_CMD;
	p_ExMem++;
	*p_ExMem = (memBank << 6) | (ptr.ebv[0]) >> 2;
	p_ExMem++;
	if (ptr.byte_length == 1)
	{
		*p_ExMem = (ptr.ebv[0] << 6) | (XORedData[0] >> 2);
		p_ExMem++;
	}
	else if (ptr.byte_length == 2)
	{
		*p_ExMem = (ptr.ebv[0] << 6) | (ptr.ebv[1] >> 2);
		p_ExMem++;
		*p_ExMem = (ptr.ebv[1] << 6) | (XORedData[0] >> 2);
		p_ExMem++;
	}
	*p_ExMem = (XORedData[0] << 6) | (XORedData[1] >> 2);
	p_ExMem++;
	*p_ExMem = (XORedData[1] << 6) | (HANDLEH >> 2);
	p_ExMem++;
	*p_ExMem = (HANDLEH << 6) | (HANDLEL >> 2);
	p_ExMem++;
	*p_ExMem = HANDLEL << 6;
	DOUBLE_POWTH;
	STARTOPT(USER_DEFINE_OPT);
	while( !(MACRODONE) );
	delayus(100);
	T1ARGU1 = 0x00;   //restore T1 to default value
	HALF_POWTH;
	if (MACRO_RXDONE && !MACRO_CRCERR)
	{
		if (!MACRO_HEADERFLAG)
		{
			MACROEN = 0;
			return 0;
		}
		else if (MACRO_HEADERFLAG)
		{
			MACROEN = 0;
			p_ExMem = 0;
			return (FAIL_WRITE_ERROR_CODE_BASE | (*p_ExMem));
		}
	}
	MACROEN = 0;
	return FAIL_WRITE_MEMORY_NO_TAG;
}

void InventoryWrite(uchar * msgBuf)
{
	xdata uint apH = (msgBuf[5] << 8) | msgBuf[6];
	xdata uint apL = (msgBuf[7] << 8) | msgBuf[8];
	xdata uchar memBank = msgBuf[9];
	xdata uint  wordPtr = (msgBuf[10] << 8) | msgBuf[11];
	xdata uchar wordCount1 = msgBuf[12]; // max write length must be smaller than 255 , wordCount1 should always be 0
	xdata uchar wordCount0 = msgBuf[13];
	xdata uint writeData;
	xdata uchar errCnt;
	xdata uchar errcode = 0;
	uchar i;
	if (InventoryAccess(apH, apL, FAIL_WRITE_MEMORY_NO_TAG))
	{
		for (i = 0; i < wordCount0; i++)
		{
			errCnt = 0;
			writeData = msgBuf[14 + (i<<1)] << 8 | msgBuf[15 + (i<<1)];  // single word one time
			while (errCnt < 5)
			{
				delayus(100);
				errcode = Write(memBank, wordPtr, writeData);
				if (0 == errcode)
				{
					break;
				}
				else  // read back to check if the write operation failed
				{
					delayus(100);
					if (0 == Read(memBank, wordPtr, 1))
					{
						p_ExMem = 0;
						if (*p_ExMem == MSB(writeData) && *(p_ExMem + 1) == LSB(writeData))
						{
							break;
						}
					}
				}
				errCnt++;
			}
			wordPtr++;
			if (errCnt >= 5)
			{
				ClosePA();
				fhss_status = FHSS_DONE;
				send_fail(errcode, PCEPC_LENGTH);
				return;
			}
		}
		ClosePA();
		fhss_status = FHSS_DONE;
		send_success(CMD_WRITE_DATA, PCEPC_LENGTH);
	}
}

uchar Lock(lock_payload_type payload)
{
	CMDLEN = 44; // CMD/8 + PAYLOAD/20 + handle/16
	CMDARGU = RES_DO_CRC16 | RES_HAS_HEADER | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE;// frame-sync, CRC-16 , response has header and check CRC-16
	TAGRSPLEN0 = 16; // handle/16
    CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set respones length high 4 bits to 0
	T1ARGU1 = LONG_T1_TIME_H;   // for 20ms delay
	DOUBLE_POWTH; // increase power threshold to avoid noise during 20ms receive time
	p_ExMem = 0;
	*p_ExMem = TYPEC_LOCK_CMD;
	p_ExMem++;
	*p_ExMem = (payload.byte0 << 4) | (payload.byte1 >> 4);
	p_ExMem++;
	*p_ExMem = (payload.byte1 << 4) | (payload.byte2 >> 4);
	p_ExMem++;
	*p_ExMem = (payload.byte2 << 4) | (HANDLEH >> 4);
	p_ExMem++;
	*p_ExMem = (HANDLEH << 4) | (HANDLEL >> 4);
	p_ExMem++;
	*p_ExMem = HANDLEL << 4;

	STARTOPT(USER_DEFINE_OPT);
	EX0 = 0;
	while (!MACRODONE);
	delayus(100);
	T1ARGU1 = 0x00;   //restore T1 to default value
	HALF_POWTH;
	if (MACRO_RXDONE && !MACRO_CRCERR)
	{
		if (!MACRO_HEADERFLAG)
		{
			MACROEN = 0;
			return 0; // success
		}
		else if (MACRO_HEADERFLAG)
		{
			MACROEN = 0;
			p_ExMem = 0;
			return (FAIL_LOCK_ERROR_CODE_BASE | (*p_ExMem));
		}
	}
	MACROEN = 0;
	return FAIL_LOCK_NO_TAG;
}

void InventoryLock(uchar * msgBuf)
{
	xdata uint apH = (msgBuf[5] << 8) | msgBuf[6];
	xdata uint apL = (msgBuf[7] << 8) | msgBuf[8];
	xdata lock_payload_type payload;
	
	xdata uchar errcode = 0;

	payload.byte0 = msgBuf[9];
	payload.byte1 = msgBuf[10];
	payload.byte2 = msgBuf[11];
	
	if (InventoryAccess(apH, apL, FAIL_LOCK_NO_TAG))
	{
		errcode = Lock(payload);
		ClosePA();
		fhss_status = FHSS_DONE;
		if (0 != errcode)
		{
			send_fail(errcode, PCEPC_LENGTH);
		}
		else
		{
			send_success(CMD_LOCK_UNLOCK, PCEPC_LENGTH);
		}
	}
}

void BuildKillFrame(uchar * killData, uchar rfu, uchar step)
{
	CMDLEN = 43;   // CMD/8 + PASSWORD/16 + RFU/3 + handle/16
	if (step == 1)
	{
		CMDARGU = RES_DO_CRC16 | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE;// frame-sync, CRC-16 , response check CRC-16
	}
	else
	{
		CMDARGU = RES_DO_CRC16 | RES_HAS_HEADER | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE;// frame-sync, CRC-16 , response has header and check CRC-16
		T1ARGU1 = LONG_T1_TIME_H;   // for 20ms delay
	}
	TAGRSPLEN0 = 16;   //handle/16
    CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set respones length high 4 bits to 0

	p_ExMem = 0;
	*p_ExMem = TYPEC_KILL_CMD;
	p_ExMem++;
	*p_ExMem = killData[0];
	p_ExMem++;
	*p_ExMem = killData[1];
	p_ExMem++;
	*p_ExMem = rfu << 5 | HANDLEH >> 3;
	p_ExMem++;
	*p_ExMem = HANDLEH << 5 | HANDLEL >> 3;
	p_ExMem++;
	*p_ExMem =  HANDLEL << 5;

}

uchar Kill(uint kpH, uint kpL,uchar rfuRecom)
{
	uchar killData[2];
	uchar newRN16H;
	uchar newRN16L;
	//1. Req_RN
	if (Req_RN(&newRN16H, &newRN16L))
	{
		killData[0] = newRN16H ^ MSB(kpH);
		killData[1] = newRN16L ^ LSB(kpH);
		BuildKillFrame(killData, 0x00, (uchar)1);
		STARTOPT(USER_DEFINE_OPT);
		while(!MACRODONE);
		delayus(200);
		p_ExMem = 0;
		//check Kill Step 1  response
		if((MACRO_RXDONE && !MACRO_CRCERR) && (*p_ExMem == HANDLEH) && (*(p_ExMem+1) == HANDLEL))
		{
			MACROEN = 0;
			//Kill step2
			//3.issue Req_RN
			if (Req_RN(&newRN16H, &newRN16L))
			{
				killData[0] = newRN16H ^ MSB(kpL);
				killData[1] = newRN16L ^ LSB(kpL);
				//4.issue Kill command
				BuildKillFrame(killData, rfuRecom, 2);
				DOUBLE_POWTH; // increase power threshold to avoid noise during 20ms receive time
				STARTOPT(USER_DEFINE_OPT);  //
				while (!MACRODONE);
				p_ExMem = 0;
				T1ARGU1 = 0x00;   //resotre T1 to default value
				HALF_POWTH;
				//check Kill Step 2 response
				if (MACRO_RXDONE && !MACRO_CRCERR)
				{
					if (!MACRO_HEADERFLAG)
					{
						MACROEN = 0;
						if ((*p_ExMem == HANDLEH) && (*(p_ExMem+1) == HANDLEL))
						{
							return 0;
						}
						return FAIL_KILL_NO_TAG;
					}
					else
					{
						MACROEN = 0;
						p_ExMem = 0;
						return (FAIL_KILL_ERROR_CODE_BASE | (*p_ExMem));
					}
				}
			}
		}
	}
	MACROEN = 0;
	return FAIL_KILL_NO_TAG;
}

void InventoryKill(uchar *msgBuf)
{
	xdata uint killPwH = (msgBuf[5] << 8) | msgBuf[6];
	xdata uint killPwL = (msgBuf[7] << 8) | msgBuf[8];
	uchar errCnt = 0;
	xdata uchar rfuRecom;
	uchar errcode = 0;
	if (CheckFhssStatus() != FHSS_WORK_SLOT_AVAILABLE)
	{
		send_fail(FAIL_FHSS_FAIL, 0);
		return;
	}
	fhss_status = OPT_WORKING;
    OpenPA();
	delayus(660);
	while (errCnt < 5)
	{
		if (inventoryMode != INVENTORY_MODE1)
		{
			Select();
		}
		IVRARGU = INVENTORY_MASK_NO_NAK | INVENTORY_MASK_REQ_RN;
		STARTOPT(INVENTORY_OPT);
		while(!MACRODONE);
		MACROEN = 0;
		if (EPCNUM > 0)
		{
			StorePcEpc();
			break;
		}
		errCnt++;
	}
	if (EPCNUM > 0)
	{
		delayus(30);
		if (msgBuf[4] == 4)   // not receive rfu/recom parameter
		{
			rfuRecom = 0x00;  // set default rfuRecom as 000
		}
		else
		{
			rfuRecom = msgBuf[9];
		}
		errcode = Kill(killPwH, killPwL, rfuRecom);
		ClosePA();
		fhss_status = FHSS_DONE;
		if (0 != errcode)
		{
			send_fail(errcode, PCEPC_LENGTH);
		}
		else
		{
			send_success(CMD_KILL, PCEPC_LENGTH);
		}
		return;
	}
	MACROEN = 0;
	ClosePA();
	fhss_status = FHSS_DONE;
	send_fail(FAIL_KILL_NO_TAG, 0);
}
