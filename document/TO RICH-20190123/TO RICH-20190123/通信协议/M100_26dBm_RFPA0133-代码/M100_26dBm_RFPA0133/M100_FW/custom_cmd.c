#include "typec_cmd.h"
#include "custom_cmd.h"
#include "uart.h"
#include "fhss_lbt.h"

#ifdef _FEATURE_NXP_TAG_CMDS_

// the alarm code of NXP G2X tag is always these 8 bytes
code uchar NXP_64bit_alarm_code[] = {
	0x69, 0x0A, 0xEC, 0x7C, 0xD2, 0x15, 0xD8, 0xF9
};

uchar ChangeConfig(uchar configData[2])
{
	uchar newRN16H;
	uchar newRN16L;
	uchar XORedData[2];
	EX0= 0;  //close Macro Done interrupt
	delayus(5);
	//1. Req_RN
	if (!Req_RN(&newRN16H,&newRN16L))
	{
		return FAIL_NXP_CHANGE_CONFIG_NO_TAG;
	}
	delayus(5);
	XORedData[0] = configData[0] ^ newRN16H;
	XORedData[1] = configData[1] ^ newRN16L;
	CMDLEN = 56;	  //CMD/16 + RFU/8 + Data/16 + handle/16
	CMDARGU = RES_DO_CRC16 | RES_HAS_HEADER | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE;// frame-sync, CRC-16 , response has header and check CRC-16
	TAGRSPLEN0 = 32;  // StatusBits/16 + handle/16, header and CRC checker result will be saved to SFR
	CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set respones length high 4 bits to 0
	p_ExMem = 0;
	*p_ExMem = NXP_G2X_CHANGE_CONFIG_CMD_H;
	p_ExMem++;
	*p_ExMem = NXP_G2X_CHANGE_CONFIG_CMD_L;
	p_ExMem++;
	*p_ExMem = NXP_G2X_CHANGE_CONFIG_RFU;
	p_ExMem++;
	*p_ExMem = XORedData[0];
	p_ExMem++;
	*p_ExMem = XORedData[1];
	p_ExMem++;
	*p_ExMem = HANDLEH;
	p_ExMem++;
	*p_ExMem = HANDLEL;
	T1ARGU1 = LONG_T1_TIME_H;   // for 20ms delay
	DOUBLE_POWTH; // increase power threshold to avoid noise during 20ms receive time
	STARTOPT(USER_DEFINE_OPT);
	while( !(MACRODONE) );
	delayus(20);
	T1ARGU1 = 0x00;   //restore T1 to default value
	HALF_POWTH;
	p_ExMem = 0;
	if (MACRO_RXDONE && !MACRO_CRCERR)
	{
		if (!MACRO_HEADERFLAG)
		{
			MACROEN = 0;
			configData[0] = *p_ExMem ;
			configData[1] = *(p_ExMem + 1);
			return 0;
		}
		MACROEN = 0;
		return (FAIL_CUSTOM_CMD_BASE | (*p_ExMem));
	}
	MACROEN = 0;
	return FAIL_NXP_CHANGE_CONFIG_NO_TAG;
}

void InventoryChangeConfig(uchar * msgBuf)
{
	xdata uint apH = (msgBuf[5] << 8) | msgBuf[6];
	xdata uint apL = (msgBuf[7] << 8) | msgBuf[8];
	uchar configData[2];
	uchar xdata * p_StoredPcEpc = PCEPC_STORED_START_ADDR;
	uchar PCEPCLength;
	uchar i;

	uchar errCnt;
	uchar errcode = 0;
	configData[0] = msgBuf[9];
	configData[1] = msgBuf[10];
	if (InventoryAccess(apH, apL, FAIL_NXP_CHANGE_CONFIG_NO_TAG))
	{
		errCnt = 0;
		while (errCnt < 5)
		{
			delayus(100);
			errcode = ChangeConfig(configData);
			if (0 == errcode)
			{
				break;
			}
			errCnt++;
		}
		ClosePA();
		fhss_status = FHSS_DONE;
		if (errCnt >= 5)
		{
			send_fail(errcode, PCEPC_LENGTH);
		}
		else
		{
			PCEPCLength = ((*(p_StoredPcEpc) >> 3) + 1) << 1;
			resDat.byteLen = 2 + 1 + PCEPCLength;
			resDat.dat[0] = PCEPCLength;
			for (i = 1; i <= PCEPCLength; i++)
			{
				resDat.dat[i] = *p_StoredPcEpc;
				p_StoredPcEpc++;
			}
			resDat.dat[PCEPCLength + 1] = configData[0];
			resDat.dat[PCEPCLength + 2] = configData[1];
			send_response(CMD_NXP_CHANGE_CONFIG, &resDat);
		}
	}
}

uchar ReadProtect()
{
	CMDLEN = 32;	  //CMD/16 + handle/16
	CMDARGU = RES_DO_CRC16 | RES_HAS_HEADER | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE;// frame-sync, CRC-16 , response has header and check CRC-16
	TAGRSPLEN0 = 16;  // handle/16, header and CRC checker result will be saved to SFR
	CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set respones length high 4 bits to 0
	p_ExMem = 0;
	*p_ExMem = NXP_G2X_READPROTECT_CMD_H;
	p_ExMem++;
	*p_ExMem = NXP_G2X_READPROTECT_CMD_L;
	p_ExMem++;
	*p_ExMem = HANDLEH;
	p_ExMem++;
	*p_ExMem = HANDLEL;
	T1ARGU1 = LONG_T1_TIME_H;   // for 20ms delay
	DOUBLE_POWTH; // increase power threshold to avoid noise during 20ms receive time
	STARTOPT(USER_DEFINE_OPT);
	while( !(MACRODONE) );
	delayus(20);
	T1ARGU1 = 0x00;   //restore T1 to default value
	HALF_POWTH;
	if (MACRO_RXDONE && !MACRO_CRCERR)
	{
		if (!MACRO_HEADERFLAG)
		{
			MACROEN = 0;
			return 0;
		}
		MACROEN = 0;
		return (FAIL_CUSTOM_CMD_BASE | (*p_ExMem));
	}
	MACROEN = 0;
	return FAIL_NXP_READPROTECT_NO_TAG;

}

uchar ResetReadProtect(uint AccessPwd_H, uint AccessPwd_L)
{
	uchar newRN16H;
	uchar newRN16L;
	//1. Req_RN
	if (!Req_RN(&newRN16H,&newRN16L))
	{
		return FAIL_NXP_RESET_READPROTECT_NO_TAG;
	}
	CMDLEN = 64;	  //CMD/16 + AccessPwd/32 + handle/16
	CMDARGU = RES_DO_CRC16 | RES_HAS_HEADER | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE;// frame-sync, CRC-16 , response has header and check CRC-16
	TAGRSPLEN0 = 16;  // handle/16, header and CRC checker result will be saved to SFR
	CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set respones length high 4 bits to 0
	p_ExMem = 0;
	*p_ExMem = NXP_G2X_RESET_READPROTECT_CMD_H;
	p_ExMem++;
	*p_ExMem = NXP_G2X_RESET_READPROTECT_CMD_L;
	p_ExMem++;
	*p_ExMem = MSB(AccessPwd_H) ^ newRN16H;
	p_ExMem++;
	*p_ExMem = LSB(AccessPwd_H) ^ newRN16L;
	p_ExMem++;
	*p_ExMem = MSB(AccessPwd_L) ^ newRN16H;
	p_ExMem++;
	*p_ExMem = LSB(AccessPwd_L) ^ newRN16L;
	p_ExMem++;
	*p_ExMem = HANDLEH;
	p_ExMem++;
	*p_ExMem = HANDLEL;
	T1ARGU1 = LONG_T1_TIME_H;   // for 20ms delay
	DOUBLE_POWTH; // increase power threshold to avoid noise during 20ms receive time
	STARTOPT(USER_DEFINE_OPT);
	while( !(MACRODONE) );
	delayus(20);
	T1ARGU1 = 0x00;   //restore T1 to default value
	HALF_POWTH;
	if (MACRO_RXDONE && !MACRO_CRCERR)
	{
		if (!MACRO_HEADERFLAG)
		{
			MACROEN = 0;
			return 0;
		}
		MACROEN = 0;
		return (FAIL_CUSTOM_CMD_BASE | (*p_ExMem));
	}
	MACROEN = 0;
	return FAIL_NXP_RESET_READPROTECT_NO_TAG;
}

void InventoryReadProtect(uchar * msgBuf)
{
	xdata uint apH = (msgBuf[5] << 8) | msgBuf[6];
	xdata uint apL = (msgBuf[7] << 8) | msgBuf[8];
	xdata uchar reset = msgBuf[9];
	uchar errCnt;
	uchar errcode = 0;
	BOOL accessResult = FALSE;
	if (reset)
	{
		accessResult = InventoryAccess(apH, apL, FAIL_NXP_RESET_READPROTECT_NO_TAG);
	}
	else
	{
		accessResult = InventoryAccess(apH, apL, FAIL_NXP_READPROTECT_NO_TAG);
	}
	if (accessResult)
	{
		delayus(5);
		errCnt = 0;
		while(errCnt < 5)
		{
			if (reset) // Reset ReadProtect don't need Access because the command can be executed on Open State
			{
				errcode = ResetReadProtect(apH,apL);
			}
			else
			{
				errcode = ReadProtect();
			}
			if (0 == errcode)
			{
				break;
			}
			errCnt++;
		}
		ClosePA();
		fhss_status = FHSS_DONE;
		if (errCnt >= 5)
		{
			send_fail(errcode, PCEPC_LENGTH);
			return;
		}
		if (reset)
		{
			send_success(CMD_NXP_RESET_READPROTECT, PCEPC_LENGTH);
		}
		else
		{
			send_success(CMD_NXP_READPROTECT, PCEPC_LENGTH);
		}
	}
}

uchar ChangeEas(uchar set)
{
	MACROEN = 0;
	CMDLEN = 33;	  //CMD/16 + ChangeEas/1 + handle/16
	CMDARGU = RES_DO_CRC16 | RES_HAS_HEADER | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE;// frame-sync, CRC-16 , response has header and check CRC-16
	TAGRSPLEN0 = 16;  // handle/16, header and CRC checker result will be saved to SFR
	CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set respones length high 4 bits to 0
	p_ExMem = 0;
	*p_ExMem = NXP_G2X_CHANGE_EAS_CMD_H;
	p_ExMem++;
	*p_ExMem = NXP_G2X_CHANGE_EAS_CMD_L;
	p_ExMem++;
	*p_ExMem = (set << 7) | (HANDLEH >> 1);
	p_ExMem++;
	*p_ExMem = (HANDLEH << 7) | (HANDLEL >> 1);
	p_ExMem++;
	*p_ExMem = (HANDLEL << 7);
	T1ARGU1 = LONG_T1_TIME_H;   // for 20ms delay
	DOUBLE_POWTH; //increase power threshold to avoid noise during 20ms receive time
	STARTOPT(USER_DEFINE_OPT);
	while( !(MACRODONE) );
	delayus(20);
	T1ARGU1 = 0x00;       //restore T1 to default value
	HALF_POWTH;
	if (MACRO_RXDONE && !MACRO_CRCERR)
	{
		if (!MACRO_HEADERFLAG)
		{
			MACROEN = 0;
			return 0;
		}
		MACROEN = 0;
		p_ExMem = 0;
		return (FAIL_CUSTOM_CMD_BASE | (*p_ExMem));
	}
	else
	{
		delayus(50);
		if (0 == Read(MEM_BANK_EPC, NXP_G2X_CONFIG_WORD_POINTER, 1))
		{
			p_ExMem = 1;
			if (set == ((*p_ExMem) & 0x01))
			{
				return 0;
			}
			return FAIL_NXP_CHANGE_EAS_NO_TAG;
		}
	}
	MACROEN = 0;
	return FAIL_NXP_CHANGE_EAS_NO_TAG;
}

void InventoryChangeEas(uchar * msgBuf)
{
	xdata uint apH = (msgBuf[5] << 8) | msgBuf[6];
	xdata uint apL = (msgBuf[7] << 8) | msgBuf[8];
	xdata uchar setEas = msgBuf[9];

	uchar errCnt;
	uchar errcode = 0;
	if (InventoryAccess(apH, apL, FAIL_NXP_CHANGE_EAS_NO_TAG))
	{
		errCnt = 0;
		while (errCnt < 5)
		{
			delayus(100);
			errcode = ChangeEas(setEas);
			if (0 == errcode)
			{
				break;
			}
			errCnt++;
		}
		ClosePA();
		fhss_status = FHSS_DONE;
		if (errCnt >= 5)
		{
			send_fail(errcode, PCEPC_LENGTH);
		}
		else
		{
			send_success(CMD_NXP_CHANGE_EAS, PCEPC_LENGTH);
		}
	}
}

//return: the number of the bytes verified OK
uchar checkEasCode()
{
	uchar i = 0;
	p_ExMem = 0;
	/** Upon receiving an EasAlarm command, the tag first initialize the CRC5 with b01001.
	 *  While sending the EAS Code (CRC5 MSB) the CRC-5 is calculated by input of 64 bits 1 .
	 */
	for (i = 0; i < (sizeof(NXP_64bit_alarm_code) / sizeof(NXP_64bit_alarm_code[0])); i++)
	{
		if (*p_ExMem != NXP_64bit_alarm_code[i])
		{
			return i;
		}
		p_ExMem++;
	}
	return (sizeof(NXP_64bit_alarm_code) / sizeof(NXP_64bit_alarm_code[0]));
}

void EAS_Alarm()
{
	uchar i;
	uchar code_valid_cnt = 0;
	uchar try_cnt = 0;
	if (CheckFhssStatus() != FHSS_WORK_SLOT_AVAILABLE)
	{
		send_fail(FAIL_FHSS_FAIL, 0);
		return;
	}
	fhss_status = OPT_WORKING;
	OpenPA();
	delayus(660);
	EX0 = 0;

	while (try_cnt < 5)
	{
		CMDLEN = 36;	  //CMD/16 + Inv_CMD/16 + DR/1 + M/2 + TRext/1
		CMDARGU = RES_HAS_HEADER | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE | PIE_PREAMBLE;// preamble, CRC-16 , response has header and not check CRC-16
		TAGRSPLEN0 = 64;  // EAS code/64, header will be saved to SFR
		CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set response length high 4 bits to 0
		p_ExMem = 0;
		*p_ExMem = NXP_G2X_EAS_ALARM_CMD_H;
		p_ExMem++;
		*p_ExMem = NXP_G2X_EAS_ALARM_CMD_L;
		p_ExMem++;
		*p_ExMem = NXP_G2X_EAS_ALARM_INV_CMD_H;
		p_ExMem++;
		*p_ExMem = NXP_G2X_EAS_ALARM_INV_CMD_L;
		p_ExMem++;
		*p_ExMem = 0x10;   // DR must be 0(DR=8), M must be 00(M=1), TRext must be 1(Use pilot tone)

		STARTOPT(USER_DEFINE_OPT);
		while(!MACRODONE);
		delayus(50);
		if (MACRO_RXDONE && !MACRO_HEADERFLAG)
		{
			// check if all 8 bytes is same to avoid the influence of tag collision
			if (checkEasCode() >= (sizeof(NXP_64bit_alarm_code) / sizeof(NXP_64bit_alarm_code[0])))
			{
				code_valid_cnt++;
				if (code_valid_cnt > 1)
				{
					p_ExMem = 0;
					resDat.byteLen = 8;
					for (i = 0; i < 8; i++)
					{
						resDat.dat[i] = *p_ExMem;
						p_ExMem++;
					}
				}
			}
		}
		MACROEN = 0;
		try_cnt++;
	}
	ClosePA();
	fhss_status = FHSS_DONE;
	if (code_valid_cnt > 1)
	{
		send_response(CMD_NXP_EAS_ALARM, &resDat);
	}
	else
	{
		send_fail(FAIL_NXP_EAS_ALARM_NO_TAG, 0);
	}
}
#endif

#ifdef _FEATURE_MONZA_QT_CMD_
uchar Monza4QT(uchar rw, uchar persistence, uchar * payload)
{
	CMDLEN = 16 + 36; // cmd/16 + rw/1 + persistence/1 + rfu/2 + payload/16 + rn/16
	if (rw == 0) // read
	{
		TAGRSPLEN0 = 32;
	}
	else
	{
		TAGRSPLEN0 = 16;
		T1ARGU1 = LONG_T1_TIME_H;   // for 20ms delay
		DOUBLE_POWTH; //increase power threshold to avoid noise during 20ms receive time
	}
	CMDRESLEN1 = (CMDRESLEN1 & 0xF0) | 0;  //set respones length high 4 bits to 0
	CMDARGU = RES_DO_CRC16 | RES_HAS_HEADER | CMD_IS_CRC16 | CMD_HAS_CRC | CMD_HAS_RESPONSE;// frame-sync, CRC-16 , response has header and check CRC-16
	p_ExMem = 0;
	*p_ExMem = IPJ_MONZA_QT_CMD_H;
	p_ExMem++;
	*p_ExMem = IPJ_MONZA_QT_CMD_L;
	p_ExMem++;
	*p_ExMem = (rw << 7) | (persistence << 6) | (payload[0] >> 4);
	p_ExMem++;
	*p_ExMem = (payload[0] << 4) | (payload[1] >> 4);
	p_ExMem++;
	*p_ExMem = (payload[1] << 4) | (HANDLEH >> 4);
	p_ExMem++;
	*p_ExMem = (HANDLEH << 4) | (HANDLEL >> 4);
	p_ExMem++;
	*p_ExMem = (HANDLEL << 4);

	STARTOPT(USER_DEFINE_OPT);
	while( !(MACRODONE) );
	delayus(10);
	if (rw != 0) // write
	{
		T1ARGU1 = 0x00;       //restore T1 to default value
		HALF_POWTH;
	}
	if (MACRO_RXDONE && !MACRO_CRCERR)
	{
		p_ExMem = 0;
		if (MACRO_HEADERFLAG)
		{
			MACROEN = 0;
			return (FAIL_CUSTOM_CMD_BASE | (*p_ExMem));
		}
		else if (rw == 0) // QT Read
		{
			payload[0] = *p_ExMem;
			p_ExMem++;
			payload[1] = *p_ExMem;
		}
		MACROEN = 0;
		return 0;
	}
	MACROEN = 0;
	return FAIL_IPJ_MONZA_QT_NO_TAG;
}

void InventoryMonzaQT(uchar * msgBuf)
{
	xdata uint apH = (msgBuf[5] << 8) | msgBuf[6];
	xdata uint apL = (msgBuf[7] << 8) | msgBuf[8];
	xdata uchar rw = msgBuf[9]; // 0x00 means QT Read, 0x01 means QT Write
	xdata uchar persistence = msgBuf[10];
	uchar payload[2];

	uchar xdata * p_StoredPcEpc = PCEPC_STORED_START_ADDR;
	uchar PCEPCLength;
	uchar i;

	uchar errCnt;
	uchar errcode = 0;
	payload[0] = msgBuf[11];
	payload[1] = msgBuf[12];
	if (InventoryAccess(apH, apL, FAIL_IPJ_MONZA_QT_NO_TAG))
	{
		errCnt = 0;
		while (errCnt < 5)
		{
			delayus(100);
			errcode = Monza4QT(rw, persistence, payload);
			if (0 == errcode)
			{
				break;
			}
			errCnt++;
		}
		ClosePA();
		fhss_status = FHSS_DONE;
		if (errCnt >= 5)
		{
			send_fail(errcode, PCEPC_LENGTH);
		}
		else
		{
			if (rw != 0) // QT write
			{
				send_success(CMD_IPJ_MONZA_QT_WRITE, PCEPC_LENGTH);
			}
			else  // QT read
			{
				PCEPCLength = ((*(p_StoredPcEpc) >> 3) + 1) << 1;
				resDat.byteLen = 2 + 1 + PCEPCLength;
				resDat.dat[0] = PCEPCLength;
				for (i = 1; i <= PCEPCLength; i++)
				{
					resDat.dat[i] = *p_StoredPcEpc;
					p_StoredPcEpc++;
				}
				resDat.dat[PCEPCLength + 1] = payload[0];
				resDat.dat[PCEPCLength + 2] = payload[1];
				send_response(CMD_IPJ_MONZA_QT_READ, &resDat);
			}
		}
	}
}


#endif

