#ifndef CRC_H_
#define CRC_H_

#include "util.h"

unsigned int crc_ccitt16 (unsigned char *buf,unsigned char n);
BOOL CRCRight(uchar * p, unsigned char n);

#endif
