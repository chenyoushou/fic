#include <M100Core.h>
#include <intrins.h>   //to use _nop_()
#include "uart_cmd.h"
#include "util.h"
#include "typec_cmd.h"
#include "freq.h"
#include "uart.h"
#include "crc.h"
#include "config.h"
#include "timer.h"
#include "fhss_lbt.h"
#include "custom_cmd.h"

//Global Declaration
bit frameBeginFlag = 0;
bit frameEndFlag = 0;
uchar frameLength = 0;   //the length of the whole frame
uchar frameDataLen = 0;  //the length of the data field of the frame
frame_data_type xdata resDat _at_ TX_BUF_START_ADDR;   // at ex data ram 7936bytes

uchar idata rxBuf[RX_BUF_SIZE];  //rxBuf for uart data in

uint idata loopCnt;

extern uchar xdata *p_ExMem;

void IoControl(uchar param0, uchar param1, uchar param2)
{
   resDat.byteLen = 3;
   resDat.dat[0] = param0;
   resDat.dat[1] = param1;
   if (param0 == 0x00)
   {
       if (param2 == 0x00) // input
       {
           P1CON |= (0x08 << param1);
       }
       else if (param2 == 0x01) //output
       {
           P1CON &= ~(0x08 << param1);
       }
      resDat.dat[2] = 0x01;
   }
   else if (param0 == 0x01)
   {
       if (param2 == 0x00) // set IO low
       {
           P1 &= ~(0x08 << param1);
       }
       else if (param2 == 0x01) //set IO high
       {
           P1 |= (0x08 << param1);
       }
       resDat.dat[2] = 0x01;
   }
   else if (param0 == 0x02)
   {
       if (P1 & (0x08 << param1)) // read IO is high
       {
           resDat.dat[2] = 0x01;
       }
       else
       {
           resDat.dat[2] = 0x00;
       }
   }
   send_response(CMD_IO_CONTROL, &resDat);
}

void main()
{
	P1CON = ~(BIT3);   //sfr 0x93 // The IO is P1.3
	P1_3 = 1;
	ES0 = 1;
	PS0 = 1;     //uart0 high priority
	MACROEN = 0;
	EA = 1;

	loopCnt = 0;
	p_ExMem = 0;
	delayus(200);
	Timer0Init();
	InitSfr();
	InitParam();
	delayus(5000);
	P1 = 0x00;		//0x90
//	AutoInsertChannel(4, FALSE);
	SetFhss(FHSS_ON);
	while(1)
	{
		if(frameEndFlag)
	    {
			//echo();
			parse_run();	
			frameEndFlag = 0;
		}

		if (loopCnt > 0) 
		{
			InventoryOnce();
			loopCnt--;
			delayus(1000);
		}
	}
}

/*******************************************
解析PC端发送过来的数据
********************************************/
void parse_run(void)
{

	if ((rxBuf[1] == FRAME_CMD))
	{
		switch (rxBuf[2]) {
			 case CMD_HEART_BEAT:
				 SendByte(HEART_BEAT_RESPONSE); // there is only ONE byte response of CMD_HEART_BEAT!
				 break;
			case CMD_GET_MODULE_INFO:
				get_module_info(rxBuf[5]);
				break;
			case CMD_INIT_SFR:
				InitParam();
				InitSfr();
				break;
// 			case CMD_READ_MEM:
// 				ReadExMem(rxBuf[5], rxBuf[6], rxBuf[7]);
// 				break;
			case CMD_SET_REGION:
				SetRegion(rxBuf[5]);
				send_success(CMD_SET_REGION, 0);
				break;
			case CMD_GET_REGION:
				GetRegion();
				break;
			case CMD_INSERT_FHSS_CHANNEL:
				if (InsertFhssChannel(&(rxBuf[6]), rxBuf[5]))
				{
					send_success(CMD_INSERT_FHSS_CHANNEL, 0);
				}
				break;
			case CMD_SET_RF_CHANNEL:
				SetFreq(rxBuf[5], curRegion, FALSE);
				send_success(CMD_SET_RF_CHANNEL, 0);
				break;
			case CMD_SET_CHN2_CHANNEL:
				SetFreq(rxBuf[5], REGION_CHN2, FALSE);
				break;
			case CMD_SET_US_CHANNEL:
				SetFreq(rxBuf[5], REGION_US, FALSE);
				break;
			case CMD_GET_RF_CHANNEL:
				GetFreq();
				break;
			case CMD_SET_QUERY_PARA:
				SetQueryParam(rxBuf[5], rxBuf[6]);
				break;
			case CMD_GET_QUERY_PARA:
				GetQueryParam();
				break;
			case CMD_SET_FHSS:
				SetFhss(rxBuf[5]);
				send_success(CMD_SET_FHSS, 0);
				break;
//			case CMD_AUTO_ADJUST_CH:
//				AutoInsertChannel(rxBuf[5], TRUE);
//				break;
			case CMD_OPEN_PA:
				ManualSetPA();
				break;
			case CMD_SET_CW:
				if (rxBuf[5] == 0xFF)
				{
					OpenPA();
				}
				else if (rxBuf[5] == 0x00)
				{
					ClosePA();
				}
                send_success(CMD_SET_CW, 0);
				break;
			case CMD_SET_POWER:
				SetPower((rxBuf[5] << 8) | rxBuf[6]);
                send_success(CMD_SET_POWER, 0);
				break;
			case CMD_GET_POWER:
				GetPower();
				break;
			case CMD_SINGLE_ID:
				InventoryOnce();
				break;
			case CMD_MULTI_ID:
				loopCnt = rxBuf[6] << 8 | rxBuf[7];
				break;
			case CMD_STOP_MULTI:
				StopMulti();
				break;
			case CMD_READ_DATA:
				InventoryRead(rxBuf);
				break;
			case CMD_WRITE_DATA:
				InventoryWrite(rxBuf);
				break;
			case CMD_LOCK_UNLOCK:
				InventoryLock(rxBuf);
				break;
			case CMD_KILL:
				InventoryKill(rxBuf);
				break;
			case CMD_SET_SELECT_PARA:
				SetSelParam(rxBuf);
				break;
			case CMD_GET_SELECT_PARA:
				GetSelParam();
				break;
			case CMD_SET_INV_MODE:
				SetInventoryMode(rxBuf[5]);
				break;
			case CMD_SET_MODEM_PARA:
				mixerGain = rxBuf[5];
				IFAmpGain = rxBuf[6];
				signalThreshold = (rxBuf[7] << 8) | rxBuf[8];
				SetModemPara(mixerGain, IFAmpGain, signalThreshold);
				send_success(CMD_SET_MODEM_PARA, 0);
				break;
			case CMD_READ_MODEM_PARA:
				GetModemPara();
				break;
			case CMD_SET_ENV_MODE:
				SetEnvMode(rxBuf[5]);
				break;
			case CMD_SCAN_JAMMER:
				ScanJammer();
				send_response(CMD_SCAN_JAMMER, &resDat);
				break;
			case CMD_SCAN_RSSI:
				ScanRssi();
				break;

#ifdef _FEATURE_NXP_TAG_CMDS_
			case CMD_NXP_CHANGE_CONFIG:
				InventoryChangeConfig(rxBuf);
				break;
			case CMD_NXP_READPROTECT:
			case CMD_NXP_RESET_READPROTECT:
				InventoryReadProtect(rxBuf);
				break;
			case CMD_NXP_CHANGE_EAS:
				InventoryChangeEas(rxBuf);
				break;
			case CMD_NXP_EAS_ALARM:
				EAS_Alarm();
				break;
#endif

#ifdef _FEATURE_MONZA_QT_CMD_
			case CMD_IPJ_MONZA_QT_READ:
			case CMD_IPJ_MONZA_QT_WRITE:
				InventoryMonzaQT(rxBuf);
				break;
#endif

			case CMD_SET_UART_BAUDRATE:
				SetUartBaudrate((rxBuf[5] << 8) | rxBuf[6]);
				break;
			case CMD_IO_CONTROL:
				IoControl(rxBuf[5],rxBuf[6],rxBuf[7]);
				break;
			case CMD_TEST_RESET:
				ResetFW();
				break;
			case CMD_POWERDOWN_MODE:
			case CMD_SET_SLEEP_TIME:
			case CMD_RESTART:
				break;

			case CMD_LOAD_NV_CONFIG:
			case CMD_SAVE_NV_CONFIG:
				break;

			default:
				send_fail(FAIL_INVALID_CMD, 0);
				break;
		}
	}
	else if (rxBuf[1] == FRAME_ERROR)
	{
		send_fail(FAIL_INVALID_CMD, 0);
	}
}

void uart_intr() interrupt 4 using 1
{ 
	ES0 = 0 ;
	if(RI==1)
	{	
		if(frameBeginFlag)
		{
			rxBuf[frameLength] = SBUF;
			if (frameLength == 4)
			{
				rxBuf[frameLength] = SBUF;
				if (rxBuf[3] > 0)
				{
					rxBuf[1] = FRAME_ERROR;
				}
				frameDataLen = rxBuf[4];
			}
			else if (frameLength == (frameDataLen+6) )
			{
				uchar checkSum = 0;
				uchar i;
				if (rxBuf[frameLength] != FRAME_END)
				{
					//("Frame End Not found when data counter ends");
					rxBuf[1] = FRAME_ERROR;
				}
				for (i = 1; i < frameLength - 1; i++)
				{
					checkSum += rxBuf[i];
				}
				if (!(checkSum == rxBuf[frameLength-1]))
				{
					//("Frame checkSum Not Correct");
					rxBuf[1] = FRAME_ERROR;
				}
				rxBuf[frameLength] = SBUF;
				frameBeginFlag = 0;
				frameEndFlag = 1;
			}
			frameLength++;
			if (frameLength > (RX_BUF_SIZE - 1))
			{
				frameLength = 0;
				frameBeginFlag = 0;
				frameEndFlag = 1;
				frameDataLen = 0;
				rxBuf[1] = FRAME_ERROR;
			}
		}
		else if (SBUF == FRAME_HEAD && frameBeginFlag != 1)
		{
			rxBuf[0] = SBUF;
			frameLength = 1;
			frameDataLen = 0;
			frameBeginFlag = 1;
			frameEndFlag = 0;
		}
	  RI=0;
	}
	ES0 = 1;
}


void EX0_isr() interrupt 0
{
	EA = 0;
	MACROEN = 0;
	EA = 1;
}

code char hardwareVersion[] = "M100 26dBm V1.0";
code char softwareVersion[] = "V2.10";
code char manufactureInfo[] = "MagicRf";

void get_module_info(uchar type)
{
	uchar i;
	if (type == MODULE_HARDWARE_VERSION)
	{
		resDat.byteLen = sizeof(hardwareVersion);
		resDat.dat[0] = type;
		for (i = 0; i < sizeof(hardwareVersion) -1; i++)
		{
			resDat.dat[i + 1] = hardwareVersion[i];
		}
	}
	else if (type == MODULE_SOFTWARE_VERSION)
	{
		resDat.byteLen = sizeof(softwareVersion);
		resDat.dat[0] = type;
		for (i = 0; i < sizeof(softwareVersion) -1; i++)
		{
			resDat.dat[i + 1] = softwareVersion[i];
		}
	}
	else if (type == MODULE_MANUFACTURE_INFO)
	{
		//manufacture info: M a g i c R F
		resDat.byteLen = sizeof(manufactureInfo);
		resDat.dat[0] = type;
		for (i = 0; i < sizeof(manufactureInfo) -1; i++)
		{
			resDat.dat[i + 1] = manufactureInfo[i];
		}
	}
	else
	{
		return;
	}
	send_response(CMD_GET_MODULE_INFO, &resDat);
}

void send_response(uchar cmd_type, frame_data_type * dat)
{
	uchar i;
	uchar checksum;
	SendByte(FRAME_HEAD);
	SendByte(FRAME_RES);
	SendByte(cmd_type);
	SendByte(0x00);
	SendByte(dat->byteLen);
	checksum = FRAME_RES + cmd_type + dat->byteLen;
	for (i = 0; i < dat->byteLen; i++)
	{
		checksum += dat->dat[i];
		SendByte(dat->dat[i]);
	}
	SendByte(checksum);
	SendByte(FRAME_END);
}

void send_fail(uchar fail_type, uchar pcEpcLen)
{
	uchar checkSum = FRAME_RES + CMD_FAIL;
	SendByte(FRAME_HEAD);
	SendByte(FRAME_RES);
	SendByte(CMD_FAIL);
	SendByte(0x00);
	if (pcEpcLen == 0)
	{
		SendByte(0x01);
		checkSum += 0x01;
	}
	else
	{
		SendByte(2 + pcEpcLen);
		checkSum += 2 + pcEpcLen;
	}
	SendByte(fail_type);
	checkSum += fail_type;
	if (pcEpcLen != 0)
	{
		checkSum += SendPcEpc();
	}
	SendByte(checkSum);
	SendByte(FRAME_END);
}

void send_success(uchar cmd_type, uchar pcEpcLen)
{
	uchar checkSum = 0x00;
	SendByte(FRAME_HEAD);
	SendByte(FRAME_RES);
	SendByte(cmd_type); //cmd which executes successes
	SendByte(0x00);
	checkSum = FRAME_RES + cmd_type;
	if (pcEpcLen == 0)
	{
		SendByte(0x01);
		checkSum += 0x01;
	}
	else
	{
		SendByte(2 + pcEpcLen);
		checkSum += 2 + pcEpcLen;
	}
	if (pcEpcLen != 0)
	{
		checkSum += SendPcEpc();
	}
	SendByte(CMD_SUCCESS);
	SendByte(checkSum);
	SendByte(FRAME_END);
}

//void send_debug_byte(uchar debug_byte)
//{
//	SendByte(FRAME_HEAD);
//	SendByte(FRAME_RES);
//	SendByte(RESPONSE_DEBUG);
//	SendByte(0x00);
//	SendByte(0x01);
//	SendByte(debug_byte);
//	SendByte(debug_byte + 0x01 + 0x34 + 0x01);
//	SendByte(FRAME_END);
//}
