#include "fhss_lbt.h"
#include "timer.h"
#include "uart_cmd.h"
#include "typec_cmd.h"
#include "config.h"
#include "freq.h"
#include <stdlib.h>

uchar nextFreqIndex;

xdata freq_list_t freq_list;

code uchar HfssChn1OptChannel[]={
0,1,2,3,4,5,6,7,8,9,10,13,14,15,16,17,18,19
};

BOOL InsertFhssChannel(uchar * freqIndexList, uchar channelCnt)
{
	uchar i;
	uchar j;
	if (channelCnt == 0)
	{
		freq_list.chCnt = 0;
		return TRUE;
	}
	else if (channelCnt > MAX_CHANNEL_NUM)
	{
		send_fail(CMD_INSERT_FHSS_CHANNEL, 0);
		return FALSE;
	}
	for (i = 0; i < channelCnt; i++)
	{
		if (curRegion == REGION_CHN2) //try to avoid some bad frequency point
		{
			for (j = 0; j < sizeof(HfssChn1OptChannel); j++)
			{
				if (freqIndexList[i] == HfssChn1OptChannel[j])
				{
					break;
				}
			}
			if (j == sizeof(HfssChn1OptChannel)) // not found in the HfssChn1OptChannel[], so the chIndex is a bad freq point
			{                                    // should not insert to the freq_list
				continue;
			}
		}
		for (j = 0; j < freq_list.chCnt; j++)
		{
			if (freq_list.freqPoint[j] == freqIndexList[i])
			{
				break;
			}
		}
		if (j == freq_list.chCnt) // Not found a same freqPoint
		{
			freq_list.freqPoint[freq_list.chCnt] = freqIndexList[i];
			freq_list.chCnt++;
		}
	}
	return TRUE;
}

uchar GetNextCh(void)
{
	if (freq_list.chCnt == 0)
	{
		switch (curRegion)
		{
			case REGION_CHN2:
			case REGION_CHN1:
	//          return 1;
				return HfssChn1OptChannel[(rand() % sizeof(HfssChn1OptChannel))];
			break;
			case REGION_US:
				return (rand() % 52);
				break;
			case REGION_KOREA:
				return (rand()% 32);
				break;
			case REGION_EUR:
				return (rand()% 15);
				break;
			default:
				return 1;
				break;
		}
	}
	else
	{
		return freq_list.freqPoint[(rand() % freq_list.chCnt)];
	}
	return 1;
}

void LbtSensing(uchar freqIndex)
{

	uchar i = 0;
	uchar overLimitCnt = 0;
	uchar lbtRes[4];
	uchar lbtStep = 0; 
	fhss_status = LBT_WORKING;
	lbtStep	= Lbt(freqIndex, curRegion, lbtRes);

	if (lbtStep == LBT_LOW_POWER_STEP)
	{
		for (i = 0; i < 4; i++)
		{
			if (lbtRes[i] > LBT_THRESHOLD)
			{
				overLimitCnt++;
			}
		}
		if (overLimitCnt >= 3)
		{
			last_status_elapsed = 0;
			fhss_status = CW_IDLE;
			return;
		}
	}
	else if (lbtStep == LBT_HIGH_POWER_STEP)
	{
		last_status_elapsed = 0;
		fhss_status = CW_IDLE;
		return;
	}
//	last_status_elapsed = 0;
	fhss_status = LBT_IDLE;
}

void fhssLbtGetNextStatus(void)
{
	last_status_elapsed++;
	if (fhss_status == CW_IDLE)
	{
		if (last_status_elapsed >= CW_IDLE_TIME)
		{
			fhss_status = LBT_SENSING;		
			last_status_elapsed = 0;
			nextFreqIndex = GetNextCh();
			LbtSensing(nextFreqIndex);
		}
	}
	else if (fhss_status == LBT_SENSING)
	{
		if (last_status_elapsed >= LBT_SENING_TIME)
		{
			fhss_status = LBT_IDLE;
			last_status_elapsed = 0;
		}
	}
	else if (fhss_status == LBT_IDLE)
	{
		if (last_status_elapsed >= LBT_IDLE_TIME)
		{
			fhss_status = FHSS_CW_ON;
			last_status_elapsed = 0;
			SetFreq(nextFreqIndex, curRegion, FALSE);
		}
	}
	else if (fhss_status == FHSS_CW_ON)
	{
//		SetFreq(nextFreqIndex, curRegion, FALSE); // this will be called several times during FHSS_CW_ON !
		if (last_status_elapsed >= FHSS_CW_ON_TIME)
		{
			fhss_status = FHSS_DONE;
			last_status_elapsed = 0;
		}
	}
	else if (fhss_status == FHSS_DONE)
	{
		if (last_status_elapsed >= FHSS_DONE_TIME)
		{
			fhss_status = CW_IDLE;
			last_status_elapsed = 0;
		}
	}
}

fhss_work_slot_status CheckFhssStatus()
{
	uchar timeOutCnt = 0;
	while (fhss_status != FHSS_DONE)
	{
		delayus(1000);
		timeOutCnt++;
		if (timeOutCnt > 50)
		{
			return FHSS_NO_VALID_WORK_SLOT;
		}
	}
	return FHSS_WORK_SLOT_AVAILABLE;
}


