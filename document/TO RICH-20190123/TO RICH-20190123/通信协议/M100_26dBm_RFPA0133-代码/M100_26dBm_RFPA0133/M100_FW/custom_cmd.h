#ifndef _CUSTOM_CMD_H_
#define _CUSTOM_CMD_H_

#define _FEATURE_NXP_TAG_CMDS_
#define _FEATURE_MONZA_QT_CMD_

#ifdef _FEATURE_NXP_TAG_CMDS_

#define NXP_G2X_CHANGE_CONFIG_CMD           (0xE007)
#define NXP_G2X_CHANGE_CONFIG_CMD_H         (0xE0)
#define NXP_G2X_CHANGE_CONFIG_CMD_L         (0x07)
#define NXP_G2X_CHANGE_CONFIG_RFU           (0x00)

#define NXP_G2X_CONFIG_WORD_POINTER         (0x0020)

#define NXP_G2X_READPROTECT_CMD             (0xE001)
#define NXP_G2X_READPROTECT_CMD_H           (0xE0)
#define NXP_G2X_READPROTECT_CMD_L           (0x01)

#define NXP_G2X_RESET_READPROTECT_CMD       (0xE002)
#define NXP_G2X_RESET_READPROTECT_CMD_H     (0xE0)
#define NXP_G2X_RESET_READPROTECT_CMD_L     (0x02)

#define NXP_G2X_CHANGE_EAS_CMD              (0xE003)
#define NXP_G2X_CHANGE_EAS_CMD_H            (0xE0)
#define NXP_G2X_CHANGE_EAS_CMD_L            (0x03)
#define NXP_G2X_EAS_ALARM_CMD               (0xE004)
#define NXP_G2X_EAS_ALARM_CMD_H             (0xE0)
#define NXP_G2X_EAS_ALARM_CMD_L             (0x04)
#define NXP_G2X_EAS_ALARM_INV_CMD           ~(0xE004)
#define NXP_G2X_EAS_ALARM_INV_CMD_H         ~(0xE0)
#define NXP_G2X_EAS_ALARM_INV_CMD_L         ~(0x04)

void InventoryChangeConfig(uchar * msgBuf);
void InventoryReadProtect(uchar * msgBuf);
void InventoryChangeEas(uchar * msgBuf);
void EAS_Alarm();

#endif

#ifdef _FEATURE_MONZA_QT_CMD_

#define IPJ_MONZA_QT_CMD                     (0xE000)
#define IPJ_MONZA_QT_CMD_H                   (0xE0)
#define IPJ_MONZA_QT_CMD_L                   (0x00)

void InventoryMonzaQT(uchar * msgBuf);
#endif

#endif
