#ifndef TIMER_H_
#define TIMER_H_

#include <M100Core.h>
#include <intrins.h>   //to use _nop_()
#include "util.h"
#include "freq.h"
#include "uart.h"

#define uchar unsigned char
#define uint  unsigned int

//time unit 0.1ms
#define CW_IDLE_TIME (100)
#define FHSS_DONE_TIME (1000)
#define FHSS_CW_ON_TIME (35)
#define LBT_IDLE_TIME (20)
#define LBT_SENING_TIME (100)

extern uint idata time_tick;  // ervery 0.1ms increase
extern uint idata last_status_elapsed;
extern uchar idata fhss_status;

#define FHSS_WAIT_NEXT (1)
#define CW_IDLE        (2)
#define LBT_SENSING    (3)
#define LBT_IDLE       (4)
#define FHSS_CW_ON     (5)
#define FHSS_DONE      (6)
#define OPT_WORKING    (7)
#define OPT_DONE       (8)
#define LBT_WORKING    (9)

void Timer0Init(void);
void SetTimeOut(uchar us10);
#endif
