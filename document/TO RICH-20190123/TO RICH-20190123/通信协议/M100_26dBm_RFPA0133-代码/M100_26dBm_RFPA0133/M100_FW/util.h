#ifndef UTIL_H_
#define UTIL_H_

#define  uchar     unsigned  char
#define  uint      unsigned  int

#define  uint8   unsigned  char
#define  uint16  unsigned  int

#ifdef LITTLE_ENDIAN
#define MSB(u16)        (((uint8* )&u16)[1])
#define LSB(u16)        (((uint8* )&u16)[0])
#else
#define MSB(u16)        (((uint8* )&u16)[0])
#define LSB(u16)        (((uint8* )&u16)[1])
#endif

#define REGION_CHN2  (1)
#define REGION_CHN1  (4)
#define REGION_US    (2)
#define REGION_EUR   (3)
#define REGION_JAPAN (5)
#define REGION_KOREA (6)

#define BIT0          (0x01)
#define BIT1          (0x01 << 1)
#define BIT2          (0x01 << 2)
#define BIT3          (0x01 << 3)
#define BIT4          (0x01 << 4)
#define BIT5          (0x01 << 5)
#define BIT6          (0x01 << 6)
#define BIT7          (0x01 << 7)
 
typedef unsigned char BOOL;
#define TRUE (1==1)
#define FALSE (0==1)

typedef union
{
    unsigned long l;
    unsigned char c[4];
} long_t;

#define LBT_HIGH_POWER_STEP	(1)
#define LBT_LOW_POWER_STEP (2)

void delayus(uint us);
void ResetFW(void);
char Abs(char d);

#endif
