#ifndef __FHSS_LBT_H_
#define __FHSS_LBT_H_
#include "timer.h"
#include "util.h"

#define LBT_THRESHOLD (0x06)

typedef struct
{
	uchar chCnt;
	uchar freqPoint[MAX_CHANNEL_NUM];
} freq_list_t;

extern xdata freq_list_t freq_list;

typedef enum {
	FHSS_WORK_SLOT_AVAILABLE = 0,
	FHSS_NO_VALID_WORK_SLOT = 1
} fhss_work_slot_status;

BOOL InsertFhssChannel(uchar * freqIndexList, uchar channelCnt);
void fhssLbtGetNextStatus(void);
void LbtSensing(uchar freqIndex);
fhss_work_slot_status CheckFhssStatus();

#endif
