#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "util.h"
#include "freq.h"

void InitSfr(void);
BOOL XtalCal(void);
uchar Lbt(uchar freqIndex, uchar region,uchar * senseRes);
void SetModemPara(uchar mixerGain,uchar IFAmpGain,uint signalThreshold);
char GetJammer();
char GetRssi();

#endif
