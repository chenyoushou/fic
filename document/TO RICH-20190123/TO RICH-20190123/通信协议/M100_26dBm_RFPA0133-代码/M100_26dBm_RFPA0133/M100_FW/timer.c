#include "timer.h"
#include "util.h"
#include "freq.h"
#include "uart.h"
#include "TYPEC_CMD.H"
#include "fhss_lbt.h"

//uint idata time_tick;  // ervery 0.1ms increase
uint idata last_status_elapsed;

uchar idata fhss_status;

//Timer 0 initialize
//set interval to 0.1ms
void Timer0Init(void)
{
	TMOD |= 0x02; 		//Timer 0 Mode 1,8-bit auto-reload	
	CKCON &= 0xF7 ;     // driver by clk/12
	TH0 = 0xFF - 0xD9;
//	ET0 = 1;			//open Timer 0 interupt
//	TR0 = 1;
}

void T0_isr() interrupt 1
{
	ET0 = 0;
//	time_tick++;
	fhssLbtGetNextStatus();
	ET0 = 1;
}
