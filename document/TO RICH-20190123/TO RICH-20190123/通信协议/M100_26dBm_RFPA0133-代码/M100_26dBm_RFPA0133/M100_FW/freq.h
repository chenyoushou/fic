#ifndef FREQ_H_
#define FREQ_H_

#include "util.h"

#define MAX_CHANNEL_NUM  (52)

extern uchar curRegion;
extern uchar curFreqIndex;

void SetRegion(uchar region);
uchar GetRegion(void);
void SetFreq(uchar index, uchar region, BOOL isLbt);
uchar GetFreq();

#endif
