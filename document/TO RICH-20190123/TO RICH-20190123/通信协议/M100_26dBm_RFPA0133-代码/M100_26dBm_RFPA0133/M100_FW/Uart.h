#ifndef UART_H_
#define UART_H_

#include "M100Core.h"

#define uchar unsigned char
#define uint  unsigned int

/*********************************************
Abstract   : Serial Port 0 initialize
Date:      : 2-24-2011
Description: Baudrate = 9600, using Timer 0
*********************************************/
void Uart0_ini();

/******************************************************************
* Abstract��SendByte
* description: Send a byte by Uart
* parameter: dat, data to send
* return: none
******************************************************************/
void SendByte(uchar dat);
void SetUartBaudrate(uint baudrate);

#endif
