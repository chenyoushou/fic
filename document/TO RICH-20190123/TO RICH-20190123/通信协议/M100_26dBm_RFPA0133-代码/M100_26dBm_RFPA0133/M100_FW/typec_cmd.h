#ifndef _TYPEC_CMD_H_
#define _TYPEC_CMD_H_

#include "M100Core.h"
#include "util.h"
#include "uart_cmd.h"
#include "freq.h"
#include "timer.h"
#ifdef _FEATURE_TRUNCATED_
#include "crc.h"
#endif

#define RSSI_STORE

#define SELECT       0xA0
/*  Inventory Set(Query,QueryAdjust,QueryRep,ACK,NAK) */
#define INVENTORY_ONCE 0x01
#define INVENTORY_SET 0x20	      
#define QUERY        0x80
#define QUERYADJUST  0x90
#define QUERYREP     0x00
#define ACK          0x40
#define NAK          0xC0

/*  Access Set(Req_RN,Read,Write,Kill,Lock...)  */
#define REQ_RN       			0xC1
//Read after inventory
#define INVENTORY_READ         	0xF2  
#define READ         			0xC2

#define INVENTORY_WRITE        	0xF3
#define WRITE        			0xC3

#define INVENTORY_ACCESS       	0xF4
#define ACCESS       			0xC4

#define KILL         			0xC5
#define LOCK         			0xC6

#define TYPEC_SEL_CMD					(0xA0)
#define TYPEC_QRY_CMD 					(0x08)

#define TYPEC_QRYADJ_CMD				(0x09)

#define TYPEC_QRYREP_CMD				(0x00)
#define TYPEC_ACK_CMD					(0x01)

#define TYPEC_NACK_CMD					(0xC0)

#define	TYPEC_REQ_RN_CMD				(0xC1)
#define TYPEC_READ_CMD					(0xC2)
#define TYPEC_WRITE_CMD					(0xC3)
#define	TYPEC_KILL_CMD					(0xC4)
#define	TYPEC_LOCK_CMD					(0xC5)
#define	TYPEC_ACCESS_CMD				(0xC6)

#define INVENTORY_MASK_NO_NAK           (0x10)
#define INVENTORY_MASK_SAVE_RSSI        (0x08)
#define INVENTORY_MASK_TRUNCATED        (0x04)
#define INVENTORY_MASK_REQ_RN           (0x01)

#define RES_DO_CRC16           (0x20)
#define RES_HAS_HEADER         (0x10)
#define CMD_IS_CRC16           (0x08)
#define CMD_HAS_CRC            (0x04)
#define CMD_HAS_RESPONSE       (0x02)
#define CMD_NO_RESPONSE        (0x00)
#define PIE_PREAMBLE           (0x01)
#define PIE_FRAME_SYNC         (0x00)

#define SINGLE_SELECT_BIT_LEN_MAX    (80)
#define SINGLE_SELECT_BYTE_LEN_MAX    (SINGLE_SELECT_BIT_LEN_MAX / 8)

#define TARGET                (0x07 << 5)
#define TARGET_S0             (0 << 5)
#define TARGET_S1             (1 << 5)
#define TARGET_S2             (2 << 5)
#define TARGET_S3             (3 << 5)
#define TARGET_SL             (4 << 5)
#define ACTION                (0x07 << 2)
#define ACTION_000            (0 << 2)
#define ACTION_001            (1 << 2)
#define ACTION_010            (2 << 2)
#define ACTION_011            (3 << 2)
#define ACTION_100            (4 << 2)
#define ACTION_101            (5 << 2)
#define ACTION_110            (6 << 2)
#define ACTION_111            (7 << 2)
#define MEM_BANK              (0x03)
#define MEM_BANK_RFU          (0)
#define MEM_BANK_EPC          (1)
#define MEM_BANK_TID          (2)
#define MEM_BANK_USER         (3)
#define PC_START_BIT_POINTER  (0x10)
#define EPC_START_BIT_POINTER  (0x20)

#define INVENTORY_OPT  0x03
#define USER_DEFINE_OPT 0x01
#define STARTOPT(opt) {BASEOPT = opt;}

#define LONG_T1_TIME_H        (0x64)
// increase power threshold to avoid noise during 20ms receive time
#define DOUBLE_POWTH          POWTH1 = (POWTH1 << 1) | (POWTH0 >> 7); \
							  POWTH0 = POWTH0 << 1;
#define HALF_POWTH            POWTH0 = (POWTH0 >> 1) | (POWTH1 << 7); \
							  POWTH1 = POWTH1 >> 1;

enum tag_error_codes {
	TAG_ERRCODE_MEM_OVERRUN = 0x03,
	TAG_ERRCODE_MEM_LOCKED  = 0x04,
	TAG_ERRCODE_INSUF_POWER = 0x0B,
	TAG_ERRCODE_OTHER_ERR   = 0x00,
	TAG_ERRCODE_NONE_SPEC   = 0x0F
};
extern uchar xdata mixerGain;
extern uchar xdata IFAmpGain;
extern uint xdata signalThreshold;

extern uint idata loopCnt;
extern uchar idata CurrentOpt;
extern uchar xdata *p_ExMem;
extern uchar idata inventoryMode;

extern uchar xdata selCombParam;
extern uchar xdata selPtr3;
extern uchar xdata selPtr2;
extern uchar xdata selPtr1;
extern uchar xdata selPtr0;
extern uchar xdata selMaskLen;
extern uchar xdata selTruncate;
extern uchar xdata selMask[32];

enum environment_mode
{
	MODE_DENSE_READER = 0x01,
	MODE_HIGH_SENSITIVITY = 0x00
};

enum inventory_mode
{
	INVENTORY_MODE0 = 0x00, // User has set Select parameter, multi-tag, Do select before every tag command
	INVENTORY_MODE1 = 0x01,	// fast tracking, Donot Select before Tag operation
	INVENTORY_MODE2 = 0x02  // User has set Select parameter, select before Read, Write, Lock, Kill except Inventory
};

#define FHSS_ON  0xFF
#define FHSS_OFF 0x00

//select mask max length is 32 bytes
#define SELECT_MASK_START_ADDR   (0x1E60)
//max PC EPC length is 512bits/64bytes
#ifdef _FEATURE_TRUNCATED_
#define TRUNCATED_EPC_START_ADDR (0x1E90)
#endif
#define PCEPC_STORED_START_ADDR    (0x1E90)
//max Tx Buffer length is 80 bytes
#define TX_BUF_START_ADDR        (0x1EE0)
//max Rx Buffer length is 190 bytes
#define RX_BUF_START_ADDR        (0x1F40)

#define PCEPC_LENGTH (( (*( (uchar xdata *)(PCEPC_STORED_START_ADDR) ) >> 3) + 1) << 1)

typedef struct {
	uchar byteLen;
	uchar pcepc[31];
} pcepc_type;

typedef struct
{
	uchar byte_length;
	uchar ebv[3]; // ebv[0] is MSB
} ebv_type;

typedef struct
{
	uchar byte0;  // high 4 bits are reserved
	uchar byte1;
	uchar byte2;
} lock_payload_type;

void OpenPA();
void ClosePA();
void SetPower(signed int power);
void GetPower();
void InitParam(void);
void AutoInsertChannel(uchar chNum, BOOL needResponse);
uchar GetChannelNum();
void SetFhss(uchar enable);
BOOL isFhssOn();
void GetModemPara();
void SetEnvMode(uchar mode);
void ScanJammer();
void ScanRssi();
void ManualSetPA(void);
void BuildEbv(uint source, ebv_type *dst);
void InventoryOnce(void);
void GetInvResult(void);
void InventoryMulti(uint loopCounter);
void StopMulti(void);
void SetQueryParam(uchar MSB, uchar LSB);
void GetQueryParam(void);
void GetSelParam(void);
void SetSelParam(uchar * rxBuf);
void SetInventoryMode(uchar mode);
void Select(void);
BOOL Req_RN(uchar * RN16H, uchar * RN16L);
uchar SendPcEpc(void);
void BuildAccessFrame(uchar *accessData);
BOOL Access(uint apH, uint apL);
BOOL InventoryAccess(uint accessPwdH, uint accessPwdL, fail_code_type default_fail_code);
uchar Read(uchar memBank, uint wordPtr, uchar wordCount);
void InventoryRead(uchar * msgBuf);
uchar Write(uchar memBank, uint wordPtr, uint writeData);
void InventoryWrite(uchar * msgBuf);
uchar Lock(lock_payload_type payload);
void InventoryLock(uchar * msgBuf);
void BuildKillFrame(uchar * killData, uchar rfu, uchar step);
uchar Kill(uint kpH, uint kpL,uchar rfuRecom);
void InventoryKill(uchar *msgBuf);

#endif


