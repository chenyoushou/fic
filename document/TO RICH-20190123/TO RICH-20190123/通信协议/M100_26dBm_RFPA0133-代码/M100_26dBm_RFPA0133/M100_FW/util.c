#include "util.h"
#include <intrins.h>   //to use _nop_()
#include "M100Core.h"
#include "typec_cmd.h"

void delayus(unsigned int us)
{
	while(--us)
	{
		_nop_();
		_nop_();
		_nop_();
		_nop_();
		_nop_();
		_nop_();
		_nop_();
	}
}

char Abs(char d)
{
	if(d < 0)
	{
		return -d;
	}
	else
	{
		return d;
	}
}

void ResetFW(void)
{
      EA = 0;
//        T2CON = 0x00;
      SetFhss(FHSS_OFF);
      #pragma asm
      JMP 0000H
      #pragma endasm
}
