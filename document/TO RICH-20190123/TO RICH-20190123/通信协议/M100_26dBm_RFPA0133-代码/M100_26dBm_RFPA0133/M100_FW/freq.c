#include "freq.h"
#include "M100Core.h"
#include "uart_cmd.h"
#include "fhss_lbt.h"

uchar curRegion;
uchar curFreqIndex;

void SetRegion(uchar region)
{
	curRegion = region;
	freq_list.chCnt = 0;
	SetFreq(1, region, FALSE);
}

uchar GetRegion(void)
{
	resDat.byteLen = 1;
	resDat.dat[0] = curRegion;
	send_response(CMD_GET_REGION,&resDat);
	return curRegion;
}

void SetFreq(uchar index, uchar region, BOOL isLbt)
{
	xdata uchar resH = 0;
	uint16 residue = 0;
	long_t res;
	xdata long_t freq;
	uchar i = 0;
    res.l = 0;
    switch (region)
    {
    	case REGION_CHN2 :
    		freq.l = 920125UL + index * 250UL;
    		break;
    	case REGION_CHN1 :
    		freq.l = 840125UL + index * 250UL;
    		break;
    	case REGION_US :
    		freq.l = 902250UL + index * 500UL;
    	    break;
    	case REGION_EUR :
    		freq.l = 865100UL + index * 200UL;
    	    break;
    	case REGION_KOREA :
    		freq.l = 917100UL + index * 200UL;
    	    break;
    	default:
    		freq.l = 921875UL;
    		break;
    }
	if (!isLbt)
	{
		curFreqIndex = index;
		resH = freq.l / 13000;
		residue = freq.l % 13000;
	}
	else
	{
		resH = (freq.l - 80) / 13000;
		residue = (freq.l - 80) % 13000;
	}
	for (i = 0; i < 20; i++) // 求余数,找分频系数
	{
		residue <<= 1;
		if (residue >= 13000)
		{
			res.l |= 1;
			residue -= 13000;
		}
		res.l <<= 1;
	}
	res.l >>= 1;

	DIVINT = resH;
	DIVFRAC_H = res.c[1];
	DIVFRAC_M = res.c[2];
	DIVFRAC_L = res.c[3];

	CALEN = CALEN & 0xF7;  //VCO CAl DISABLE
	delayus(5);
	CALEN = CALEN | 0x08;  //VCO CAl ENABLE
}

uchar GetFreq(void)
{
	resDat.byteLen = 1;
	resDat.dat[0] = curFreqIndex;
	send_response(CMD_GET_RF_CHANNEL,&resDat);
	return curFreqIndex;
}
