#include "timer.h"
#include "Common.h"

volatile uint32_t g_idleTimeSec = 0;

/****************************************
*函数名称:TMR0Init
*输    入:无
*输    出:无
*功    能:定时器0初始化
******************************************/
void TMR0Init(void)
{
    PROTECT_REG
	(
		/* 使能TMR1时钟源 */
	    APBCLK |= TMR0_CLKEN;
		/* 选择TMR1时钟源为内部22.1184MHz */  
		CLKSEL1 = (CLKSEL1 & (~TM0_CLK)) | TM0_22M; 	
	
		/* 复位TMR1 */
		IPRSTC2 |=  TMR0_RST;
		IPRSTC2 &= ~TMR0_RST;
	
		/* 选择TMR1的工作模式为周期模式*/	
		TCSR0 &= ~TMR_MODE;
		TCSR0 |=  MODE_PERIOD;		
		
		/* 溢出周期 = (Period of timer clock input) * (8-bit Prescale + 1) * (24-bit TCMP)*/
		TCSR0  = (TCSR0 & 0xFFFFFF00) | 128UL;		// 设置预分频值 [0~255] ,预分频128
//		TCMPR0 = (1327104000UL / 128UL) ;	        // 设置比较值 [0~16777215], 1327104000=22.1184M*60s
		TCMPR0 = (22118400UL / 128UL) ;	            // 设置比较值 [0~16777215], 1s one tick

		TCSR0 |= TMR_IE;					//使能TMR1中断
		NVIC_ISER |= TMR0_INT;	
	
		TCSR0 |= CRST;						//复位TMR0计数器				
		TCSR0 |= CEN;						//使能TMR0
	)
	g_idleTimeSec = 0;
}

/****************************************
*函数名称:TMR0_IRQHandler
*输    入:无
*输    出:无
*功    能:定时器0中断服务函数
******************************************/
void TMR0_IRQHandler(void)
{
    /* 清除TMR1中断标志位 */
	TISR0 |= TMR_TIF;
    g_idleTimeSec++;
    if (g_cmdInProgress)
    {
    	g_cmdTimeConsuming++;
    }
}
