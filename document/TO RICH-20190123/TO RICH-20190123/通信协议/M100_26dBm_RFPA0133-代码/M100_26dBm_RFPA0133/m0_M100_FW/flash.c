#include "Common.h"
#include "typedef.h"
#include "flash.h"

/****************************************
*函数名称:ISPTriger
*输  入:无
*输  出:无
*功  能:ISP 执行
******************************************/
VOID ISPTriger(VOID)
{
    ISPTRG |= ISPGO;
    while((ISPTRG&ISPGO) == ISPGO);
}
/****************************************
*函数名称:ISPEnable
*输  入:无
*输  出:无
*功  能:ISP 使能
******************************************/
VOID ISPEnable(VOID)
{
    Un_Lock_Reg();
    ISPCON |= ISPEN;
}
/****************************************
*函数名称:ISPDisable
*输  入:无
*输  出:无
*功  能:ISP 禁用
******************************************/
VOID ISPDisable(VOID)
{
    Un_Lock_Reg();
    ISPCON &= ~ISPEN;
}
/****************************************
*函数名称:DataFlashRWEnable
*输  入:无
*输  出:无
*功  能:数据区读写使能
******************************************/
VOID DataFlashRWEnable(VOID)
{
    Un_Lock_Reg();
    ISPCON |= LDUEN;
}
/****************************************
*函数名称:DataFlashErase
*输  入:unPage  页地址
*输  出:无
*功  能:数据区擦除
******************************************/
VOID DataFlashErase(UINT32 unPage)
{
	ISPEnable();
	DataFlashRWEnable();
	ISPCMD = PAGE_ERASE;
	ISPADR = (unPage*PAGE_SIZE+DFBADR);
	ISPTriger();
	ISPDisable(); 
}
/****************************************
*函数名称:DataFlashWrite
*输  入:pucBuf  写数据缓冲区
unSize  写数据大小
*输  出:无
*功  能:数据区写
******************************************/
VOID DataFlashWrite(UINT8 *pucBuf,UINT32 unSize)//unSize 要为 4 的倍数
{
	UINT32 i; 
	ISPEnable();
	DataFlashRWEnable();
	ISPCMD = PROGRAM;
	for(i=0; i<unSize; i+=4)
	{
		ISPADR = ((i<<2)+DFBADR); 
		ISPDAT = *(UINT32 *)(pucBuf+i);
		ISPTriger();
	}
	ISPDisable(); 
}
/****************************************
*函数名称:DataFlashRead
*输  入:pucBuf  读数据缓冲区
unSize  读数据大小
*输  出:无
*功  能:数据区读
******************************************/
VOID DataFlashRead(UINT8 *pucBuf,UINT32 unSize)
{
	UINT32 i;
	ISPEnable();
	DataFlashRWEnable();
	ISPCMD = READ;
	for(i=0; i<unSize; i+=4)
	{
		ISPADR = (i*4+DFBADR);
		ISPTriger();
		*(UINT32 *)(pucBuf+i)=ISPDAT;
	}
	ISPDisable();
}

/****************************************
*函数名称:writetoFlash
*输  入:offset  写入数据地址
				data    数据
*输  出:无
*功  能:数据区写
******************************************/
void writetoFlash(uint32_t offset,uint32_t date)
{ /*要先擦除才能写*/
//	int i;
	Un_Lock_Reg();       //往FLash写入的时候，是通过系统ISP写入，所以要先写入三个值，有例程的朋友们可以明白
	ISPCON |= ISPEN;  //使能ISP功能
	ISPCMD = PAGE_ERASE;     //选择为擦除模式
//	for(i = offset / PAGE_SIZE; i < DATAFLASH_SIZE/512; i++)    //4KB= 4096B，擦除的时候，系统规定页大小为512字节
//	{
//		ISPADR = DFBADR + i * PAGE_SIZE;
//		ISPTRG |= ISPGO;      //触发执行ISP
//		while((ISPTRG&ISPGO) == ISPGO);  //等待ISP 执行结束
//	}
	ISPCMD = PROGRAM;                 //设置为编程模式
	ISPADR = DFBADR + (offset << 2);  //得到要写的地址，等同于0x0001f000 + 4*offset,offset 就是第几个值的意思
	ISPDAT = date;                    //往数据寄存器中写入值
	ISPTRG |= ISPGO;                        //即将执行
	while((ISPTRG&ISPGO) == ISPGO);
	Un_Lock_Reg();
	ISPCON &= ~ISPEN;      //禁止ISP功能
}

/****************************************
*函数名称:readfromFlash
*输  入:offset 数据地址
*输  出:数据，32bit
*功  能:数据区读
******************************************/
uint32_t readfromFlash(uint32_t offset)
{
	uint32_t data;
	Un_Lock_Reg();           //功能如上
	ISPCON |= ISPEN;         //使能ISP功能
	ISPCMD = READ;            //设定为读模式
	ISPADR = DFBADR + (offset << 2); //得到要读的地址
    ISPTriger();
	data   = ISPDAT;               //得到数据
	Un_Lock_Reg();
	ISPCON &= ~ISPEN; //禁止ISP功能
    Lock_Reg();
	return data;
}

/****************************************
*函数名称:ConfigEnable
*输    入:无
*输    出:无
*功    能:Config使能
******************************************/
VOID ConfigEnable(VOID)
{
    Un_Lock_Reg();
    ISPCON |= CFGUEN;

}
/****************************************
*函数名称:Config0Read
*输    入:无
*输    出:无
*功    能:Config0区 读
******************************************/
UINT32 Config0Read(VOID)
{
    UINT32 unData;
    
    ISPEnable();
    ISPCMD = READ;
    ISPADR = CONFIG_START_ADDR+0x00;
    ISPTriger();
    unData = ISPDAT;
    ISPDisable();
    return unData;

}
/****************************************
*函数名称:ConfigErase
*输    入:无
*输    出:无
*功    能:Config区 擦除
******************************************/
VOID ConfigErase(VOID)
{
    ISPEnable();
    ConfigEnable();
    ISPCMD = PAGE_ERASE;
    ISPADR = CONFIG_START_ADDR;
    ISPTriger();
    ISPDisable(); 
}
/****************************************
*函数名称:Config0Write
*输    入:无
*输    出:无
*功    能:Config0区 写
******************************************/
VOID Config0Write(UINT32 unData)
{
    ISPEnable();
    ConfigEnable();
    ISPCMD = PROGRAM;
    ISPADR = CONFIG_START_ADDR+0x00;
    ISPDAT = unData;
    ISPTriger();
    ISPDisable();   
}

/****************************************
*函数名称:EnableFWIspUpdate
*输    入:isEnable, TRUE:使能固件通过ISP更新，FALSE:不使能，m0上电从APROM启动
*输    出:TRUE,使能成功; FALSE:失败
*功    能:使能固件通过ISP方式更新，即m0上电后从LDROM启动，LDROM中应存有ISP固件
******************************************/
BOOL EnableFWIspUpdate(BOOL isEnable)
{
	UINT32 oldConfig = Config0Read();
	UINT32 newConfig = 0xFFBFFFFF;
	if (isEnable)
	{
		newConfig = oldConfig & (~CONFIG_BOOT_FROM_APROM);
	}
	else
	{
		newConfig = oldConfig | CONFIG_BOOT_FROM_APROM;
	}
	ConfigErase();
	Config0Write(newConfig);
	oldConfig = Config0Read();
	if (isEnable && (oldConfig & CONFIG_BOOT_FROM_APROM) == 0)
	{
		return TRUE;
	}
	if (!isEnable && ((oldConfig & CONFIG_BOOT_FROM_APROM) != 0))
	{
		return TRUE;
	}
	return FALSE;
}

