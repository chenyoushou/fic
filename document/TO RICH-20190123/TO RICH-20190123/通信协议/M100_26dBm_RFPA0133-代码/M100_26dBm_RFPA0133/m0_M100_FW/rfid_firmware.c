#include "common.h"
#include "uart.h"
#include "flash.h"
#include "rfid_firmware.h"
#include "watchdog.h"

// if defined Macro _UART_DOWNLOAD_ , m058 will try to download firmware to the reader chip
// through uart interface. if Not defined _UART_DOWNLOAD_, m058 will try to download firmware 
// to the reader chip by IIC interface
#define _UART_DOWNLOAD_

const unsigned char firmware[] = {
#include "FW_data.h"
};

data_flash_config_t NV_config = {0,};

/*****************************************
*函数名称:I2CInit
*输    入:无
*输    出:无
*功    能:I2C初始化
******************************************/
VOID I2CInit(VOID)
{
    P3_PMD &= ~(Px4_PMD | Px5_PMD);
	P3_PMD |= (Px4_OD | Px5_OD);               	//使能I2C0引脚

    P3_MFP &= ~(P34_T0_I2CSDA | P35_T1_I2CSCL);
	P3_MFP |= (I2CSDA | I2CSCL);     			//选择P3.4,P3.5作为I2C0功能引脚   

	APBCLK |= I2C0_CLKEN;                       //使能I2C0时钟
//    I2CLK = I2C_CLOCK;                        //Slave Mode

	I2CADDR0 = 0xA0; 								// I2C slave address : 0

    I2CON |= ENSI;                             	//使能I2C
}

uint32_t Transfer_FW_IIC(void)
{
	int32_t i;
//	int32_t data_address = 0;

	I2CON |= AA;
	I2CON |= SI;
	
	while ((I2CON & SI) != SI);

	I2CON &= ((~STA)&(~SI));
	
	if (I2STATUS != 0x60)  // check if device address matches
	{
		return 0;
	}

	for (i = 0; i < 2; i++) // receive read data word address
	{
		I2CON &= ~(STA | STO);
		I2CON |= AA;
		I2CON |= SI;
		while ((I2CON & SI) != SI);
		if (I2STATUS != 0x80 && I2STATUS != 0xF8)
		{
			return 0;
		}
//		data_address = I2DAT;
	}

	I2CON |= AA;
	I2CON |= SI;

	while ((I2CON & SI) != SI);
	
	if (I2STATUS != 0xA0)  // check if repeat START
	{
		return 0;
	}

	I2CON |= SI;
	while ((I2CON & SI) != SI);
	if (I2STATUS != 0xA8)  // check if device address matches, and begin data transfer
	{
		return 0;
	}

	for (i = 0; i < sizeof(firmware); i++)
	{
//		I2CON &= ~(STA | STO | SI);
//		I2CON |= AA;
		I2DAT = firmware[i];
		I2CON |= SI;
		while ((I2CON & SI) != SI);
		if (I2STATUS == 0xA0 || I2STATUS == 0xC0) // last bit has been transfered
		{
			I2CON &= ~(STA | STO | SI | AA);
			return i;
		}
		else if (I2STATUS != 0xB8)
		{
			return 0;
		}
	}

	return 0;
}

uint32_t Download_FW(void)
{
    uint32_t i = 0;
    uint32_t tryCnt = 0;
    data_from_reader = 0;
    
    DisableWatchDog(); // disable watchdog temporary because downlaod FW will spend a long time
    Set_UART0_BAUD(__UART_CLK,9600); 

    while (data_from_reader != 0xFF)
    {
        if (tryCnt > 2000)
        {
            return 0;
        }
        Send_Data_Uart0(0xFE);
        tryCnt++;
        Delayus(3000);
    }
    
    Send_Data_Uart0(0xB5);
    
    Delayus(5000);
    
    Set_UART0_BAUD(__UART_CLK, COMMUNICATION_BAUDRATE);
    Delayus(1000);
#ifdef _UART_DOWNLOAD_
    Send_Data_Uart0(0xDB);
    Delayus(1000);
    Send_Data_Uart0(0xFD);
    Delayus(1000);
    for(i = 0; i < sizeof(firmware); i++)
    {
        Send_Data_Uart0(firmware[i]);
//      Delayus(2);
    }
#else
	I2CInit();
	Send_Data_Uart0(0xDF);
	Delayus(1000);
    Send_Data_Uart0(0xFD);
    //Delayus(1000);
	i = Transfer_FW_IIC();
#endif
    EnableWatchDog();
    FeedWatchDog();
    return i;
}

void ResetReaderChip()
{
	DISABLE_READER();
	needDownload = TRUE;
	Delayms(2);
	ENABLE_READER();
	Download_FW();
	Delayms(10); // wait reader firmware go into the parse_run loop
	ConfigReader();
	needDownload = FALSE;
}

void ConfigReader()
{
	int i;
	frame_data_t cmdDat;
	timeToSleepMin = NV_config.item.sleepTime;
	{
		byte reader_mode =  NV_config.item.reader_mode;
		cmdDat.byteLen = 1;
		cmdDat.dat[0] = reader_mode;
		send_cmd_to_reader(CMD_SET_ENV_MODE, &cmdDat);
		Delayms(10); // wait for reader firmware response, and don't send the response to PC
	}
	{
		byte region = NV_config.item.region;
		cmdDat.byteLen = 1;
		cmdDat.dat[0] = region;
		send_cmd_to_reader(CMD_SET_REGION, &cmdDat);
		Delayms(10);
	}
	{
		cmdDat.byteLen = 1 + NV_config.item.freq_list.chCnt;
		cmdDat.dat[0] = NV_config.item.freq_list.chCnt;
		for(i = 0; i < NV_config.item.freq_list.chCnt; i++)
		{
			cmdDat.dat[i + 1] = NV_config.item.freq_list.freqPoint[i];
		}
		send_cmd_to_reader(CMD_INSERT_FHSS_CHANNEL, &cmdDat);
		Delayms(10);
	}
	{
		byte Fhss_enable = NV_config.item.Fhss_enable;
		cmdDat.byteLen = 1;
		cmdDat.dat[0] = Fhss_enable;
		send_cmd_to_reader(CMD_SET_FHSS, &cmdDat);
		Delayms(10);
	}
	{
		byte freq_index = NV_config.item.freq_index;
		cmdDat.byteLen = 1;
		cmdDat.dat[0] = freq_index;
		send_cmd_to_reader(CMD_SET_RF_CHANNEL, &cmdDat);
		Delayms(10);
	}
	{
		UINT16 power = NV_config.item.power;
		cmdDat.byteLen = 2;
		cmdDat.dat[0] = (power >> 8) & 0xFF;
		cmdDat.dat[1] = power & 0xFF;
		send_cmd_to_reader(CMD_SET_POWER, &cmdDat);
		Delayms(10);
	}
}

#define NV_item_INITIALIZER { \
	.NV_enable = NV_DISABLE, \
	.sleepTime = 0, \
	.reader_mode = MODE_HIGH_SENSITIVITY, \
	.Fhss_enable = FHSS_ON, \
	.region = REGION_CHN2, \
	.freq_list.chCnt = 0 ,\
}

void Initialize_NV_item()
{
	NV_config.item.NV_enable = NV_DISABLE;
	NV_config.item.sleepTime = 0;
	NV_config.item.reader_mode = MODE_HIGH_SENSITIVITY;
	NV_config.item.Fhss_enable = FHSS_ON;
	NV_config.item.region = REGION_CHN2;
	NV_config.item.freq_list.chCnt = 0;
	NV_config.item.freq_index = 7;
	NV_config.item.power = 2000;
}

uint32_t Load_NV_config()
{
	int i;
	dword checksum;
	dword NV_enable = readfromFlash(0);
	if (NV_enable != NV_ENABLE)
	{
		return 0;
	}
	checksum = 0;
	for (i = 0; i < NV_CONFIG_DWORD_SIZE-1; i++)
	{
		NV_config.dword[i] = readfromFlash(i);
		checksum += NV_config.dword[i];
	}
	Lock_Reg();
	if (checksum != readfromFlash(NV_CONFIG_DWORD_SIZE-1))
	{
		return 0;
	}
	return sizeof(NV_config);
}

BOOL Save_NV_config(uint32_t NV_enable)
{
	int i;
	dword checksum = 0;
	NV_config.item.NV_enable = NV_enable;
    DataFlashErase(0);
    if (NV_enable != NV_ENABLE)
    {
		return TRUE;
    }
    for (i = 0; i < NV_CONFIG_DWORD_SIZE - 1; i++)
    {
		checksum += NV_config.dword[i];
		writetoFlash(i, NV_config.dword[i]);
    }
    writetoFlash(NV_CONFIG_DWORD_SIZE-1, checksum);
    Lock_Reg();
	return TRUE;
}
