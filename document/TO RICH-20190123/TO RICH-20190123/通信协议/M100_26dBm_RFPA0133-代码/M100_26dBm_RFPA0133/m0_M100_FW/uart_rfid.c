/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright(c) 2010 Nuvoton Technology Corp. All rights reserved.                                         */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

#include "Common.h"
#include "rfid_firmware.h"
#include "timer.h"
#include "typedef.h"
#include "flash.h"
#include "watchdog.h"

#define CHECK_READER_CHIP_INTERVAL    (8)

/* Global variables */                                                                                       
//volatile uint8_t comRbuf[RXBUFSIZE];
volatile uint16_t comRbytes = 0;		/* Available receiving bytes */
volatile uint16_t comRhead 	= 0;
volatile uint16_t comRtail 	= 0;
volatile int32_t g_bWait 	= 1;

volatile BOOL needDownload = TRUE;
volatile uint32_t timeToSleepMin = 0;

//Global Declaration
volatile uint8_t frameBeginFlag = 0;
volatile uint8_t frameEndFlag = 0;
volatile uint16_t frameLength;   //the length of the whole frame
volatile uint8_t rxBuf[RX_BUF_SIZE];  //rxBuf for uart data in

volatile uint8_t MY_ADDR = 0xAA;

//variable for reader chip(M100/QM100) watchdog detector
//check if the reader chip run away by sending CMD_HEART_BEAT periodically
//bit0 means got reader chip response
//bit1 means sending command to reader chip
volatile uint8_t g_readerResponseFlag = 0x01;
volatile uint32_t g_cmdTimeConsuming = 0;
volatile uint8_t g_cmdInProgress = 0;
volatile uint8_t g_heartBeatFlag = 0;

/*en:************************************************************************************
  Description:        UART0 ISR routine                                     
****************************************************************************************/
/*中:************************************************************************************
 描述 :  UART0中断代码
****************************************************************************************/
void UART0_IRQHandler(void)
{
   	uint8_t bInChar;
	
    if(UA0_ISR & RDA_INT)                              //中:检查是否接收中断//en:Check if receive interrupt
    {
        while(UA0_ISR & RDA_IF)                        //中:检查接收到的数据是否有效//en:Check if received data avaliable
        {
            while (UA0_FSR & RX_EMPTY);                //中:等字符//en:Wait until an avaliable char present in RX FIFO
            bInChar = UA0_RBR;                      //中:读取字符//en:Read the char
            
            if (!needDownload)
            {
                if ((g_readerResponseFlag & 0x02) == 0x02 && (bInChar == HEART_BEAT_RESPONSE) && (g_heartBeatFlag == 0))
                {
                    g_heartBeatFlag = 1;
                }
                else
                {
                    Send_Data_Uart1(bInChar);
                }
            }
            data_from_reader = bInChar;
            g_readerResponseFlag |= 0x01;
            g_cmdInProgress = 0;
            g_cmdTimeConsuming = 0;

// 		
//             if(comRbytes < RXBUFSIZE)                  //中:测缓冲区满否？//en:Check if buffer full
//             {
//                 comRbuf[comRtail] = bInChar[0];        //中:字符队列//en:Enqueue the character
//                 comRtail = (comRtail == (RXBUFSIZE-1)) ? 0 : (comRtail+1);
//                 comRbytes++;
//             }
        }
    }
    else if(UA0_ISR & THRE_INT)                        //中:检查是否发送中断//en:Check if transmit interrupt
    {
//         uint16_t tmp;
//         tmp = comRtail;
//         if(comRhead != tmp)
//         {
//             bInChar[0] = comRbuf[comRhead];
// 			
//             while((UA0_FSR&TX_FULL) != 0);             //中:发送FIFO满时等待//en:Wait until UART transmit FIFO is not full
//             UA0_THR = bInChar[0];                      //中:发送一个字符//en:Transmit a char via UART
//             comRhead = (comRhead == (RXBUFSIZE-1)) ? 0 : (comRhead+1);
//             comRbytes--;
//         }
    }
}

/*中:************************************************************************************
 描述 :  UART1中断代码
****************************************************************************************/
void UART1_IRQHandler(void)
{	
    uint8_t bInChar;
    if(UA1_ISR & RDA_INT)                              //中:检查是否接收中断//en:Check if receive interrupt
    {
        while(UA1_ISR & RDA_IF)                        //中:检查接收到的数据是否有效//en:Check if received data avaliable
        {
            while (UA1_FSR & RX_EMPTY);                //中:等字符//en:Wait until an avaliable char present in RX FIFO
            bInChar = UA1_RBR;                      //中:读取字符//en:Read the char

            if (!needDownload)
            {
                Send_Data_Uart0(bInChar);
            }
            
#ifdef __UART1_TO_PC___
		if(frameBeginFlag)
		{
			rxBuf[frameLength] = bInChar;
			frameLength++;
            if (frameLength > (RX_BUF_SIZE - 1))
			{
				frameLength = 0;
				frameBeginFlag = 0;
				frameEndFlag = 0;
			}
			else if ((frameLength == rxBuf[4] + 7) && frameLength > 4)
			{
				frameBeginFlag = 0;
				frameEndFlag = 1;
			}
		}
		else if (bInChar == FRAME_HEAD && frameBeginFlag != 1)
		{
			rxBuf[0] = bInChar;
			frameLength = 1;
			frameBeginFlag = 1;
			frameEndFlag = 0;
		}
#endif
            
//             if(comRbytes < RXBUFSIZE)                  //中:测缓冲区满否？//en:Check if buffer full
//             {
//                 comRbuf[comRtail] = bInChar[0];        //中:字符队列//en:Enqueue the character
//                 comRtail = (comRtail == (RXBUFSIZE-1)) ? 0 : (comRtail+1);
//                 comRbytes++;
//             }			
        }
    }
    else if(UA1_ISR & THRE_INT)                        //中:检查是否发送中断//en:Check if transmit interrupt
    {   		   
//         uint16_t tmp;
//         tmp = comRtail;
//         if(comRhead != tmp)
//         {
//             bInChar[0] = comRbuf[comRhead];
// 			
//             while((UA1_FSR&TX_FULL) != 0);             //中:发送FIFO满时等待//en:Wait until UART transmit FIFO is not full
//             UA0_THR = bInChar[0];                      //中:发送一个字符//en:Transmit a char via UART
//             comRhead = (comRhead == (RXBUFSIZE-1)) ? 0 : (comRhead+1);
//             comRbytes--;
//         }
    }
}

/*中:************************************************************************************
 描述 :  sleep模式唤醒中断代码
****************************************************************************************/
void PWRWU_IRQHandler()
{
    PWRCON |= PD_WU_STS;
    P3_MFP = (P3_MFP & (~(P31_TXD0 | P30_RXD0))) | (TXD0 | RXD0); //配置P3.1和P3.0为UART功能//en:Set P3.1 and P3.0 to UART0 function
    //enable reader module
    ENABLE_READER();
//    needDownload = TRUE;
    TMR0Init();
}

void SetReaderAddr(uint8_t isSet, uint8_t newAddr)
{
	if (isSet == 1)
	{
        if (newAddr <= 0xF0)
        {
            MY_ADDR = newAddr;
        }
	}
	resDat.byteLen = 1;
	resDat.dat[0] = MY_ADDR;
	send_response(CMD_SET_READ_ADDR, & resDat);
}

/**
 * IO1 : P2.2
 * IO2 : P2.3
 * IO3 : P2.4
 * IO4 : P2.5
 */
void IoControl(uint8_t param0, uint8_t param1, uint8_t param2)
{
	resDat.byteLen = 3;
	resDat.dat[0] = param0;
	resDat.dat[1] = param1;
	if (param0 == 0x00)
	{
		if (param2 == 0x00) // input , set PMDn to 00
		{
            P2_PMD &= ~(0x11 << ((1 + param1) << 1));
			//P1CON |= (0x08 << param1);
		}
		else if (param2 == 0x01) //output, set PMDn to 01 
		{
            P2_PMD = (P2_PMD & ~(0x11 << ((1 + param1) << 1))) | (0x01 << ((1 + param1) << 1));
			//P1CON &= ~(0x08 << param1);
		}
		resDat.dat[2] = 0x01;
	}
	else if (param0 == 0x01)
	{
		if (param2 == 0x00) // set IO low
		{
            P2_DOUT &= ~(0x01 << (1 + param1));
			//P1 &= ~(0x08 << param1);
		}
		else if (param2 == 0x01) //set IO high
		{
            P2_DOUT |= (0x01 << (1 + param1));
			//P1 |= (0x08 << param1);
		}
		resDat.dat[2] = 0x01;
	}
	else if (param0 == 0x02)
	{
		if (P2_PIN & (0x01 << (1 + param1))) // read IO is high
		{
			resDat.dat[2] = 0x01;
		}
		else
		{
			resDat.dat[2] = 0x00;
		}
	}
	send_response(CMD_IO_CONTROL, &resDat);
}

void SetPowerDownMode(BOOL needResponse)
{
    //P0_MFP = (P0_MFP & ~(P00_AD0_CTS1 | P01_AD1_RTS1))| (P00_AD0_CTS1 | P01_AD1_RTS1);
    P0_MFP |= CTS1;
    //    UA1_IER |= AUTO_CTS_EN;
    UA1_IER |= WAKE_EN;                         //enable Wake Up
    
    P2_PMD = (P2_PMD & 0xFFFFCFFF) | 0xFFFFDFFF; //Set P26 Dout mode 
    //disable reader module
    DISABLE_READER();
    needDownload = TRUE;
    
    if (needResponse)
    {
        resDat.byteLen = 1;
        resDat.dat[0] = 0; //success
        send_response(CMD_POWERDOWN_MODE, & resDat);
    }

    while (!(UA1_FSR & TX_EMPTY)); // wait for last byte sent
    Delayms(1);
    
    // 设置连接模块的串口为输入模式，减少漏电
//    P3_MFP = P3_MFP & (~(P31_TXD0 | P30_RXD0)) | (TXD0 | RXD0); //配置P3.1和P3.0为UART功能//en:Set P3.1 and P3.0 to UART0 function
    P3_MFP = (P3_MFP & (~(P31_TXD0 | P30_RXD0))) | (P30 | P31); //配置成普通IO模式
    P3_PMD = (P3_PMD & (~(Px0_PMD | Px1_PMD))) | (Px0_OUT | Px1_OUT); //配置成输出模式
    
    P3_MFP = (P3_MFP & (~(P34_T0_I2CSDA | P35_T1_I2CSCL))) | (P34 | P35); //配置IIC端口成普通IO模式
    P3_PMD = (P3_PMD & (~(Px4_PMD | Px5_PMD))) | (Px4_OUT | Px5_OUT); //配置成输出模式
    P3_DOUT = (P3_DOUT & 0xE4); //     ~(0001 1011), UART 和 IIC端口输出0
    
    Un_Lock_Reg();
    /* 禁止掉电模式下唤醒的中断中断使能 */
//    PWRCON &= ~PD_WU_IE;
    
    /*掉电模式下唤醒的中断中断使能*/
    PWRCON |= PD_WU_IE;
    
    PWRCON |= (PD_WAIT_CPU | PWR_DOWN_EN );	   /* Set power down bit */
    SCR |= SLEEPDEEP;						   /* Sleep Deep */
    Lock_Reg();  
    
    __wfi();
}

void Restart(uint8_t param0, uint8_t param1)
{
    if (param0 == 0x00 && param1 == 0xFA)
    {
        P2_PMD = (P2_PMD & 0xFFFFCFFF) | 0xFFFFDFFF; //Set P26 Dout mode 
        //disable reader module
        DISABLE_READER();
        needDownload = TRUE;
        
        send_success(CMD_RESTART);
        while((UA0_FSR & TE_FLAG) == 0); // wait for last byte sent
        Delayms(1);
        //restart m0
        PROTECT_REG
        (
            IPRSTC1 |= 0x01; // CHIP_RST
        )
        return;
    }
    send_fail(CMD_RESTART);
}

void SetSleepTime(uint8_t min)
{
    if (min <= 9)
    {
        timeToSleepMin = min;
    }
    resDat.byteLen = 1;
    resDat.dat[0] = timeToSleepMin;
    NV_config.item.sleepTime = timeToSleepMin;
    send_response(CMD_SET_SLEEP_TIME, & resDat);
}

void parse_run(void)
{
	if ((rxBuf[1] != FRAME_CMD))
	{
		return;
	}
	switch (rxBuf[2]) {
        case CMD_SET_REGION:
            NV_config.item.region = rxBuf[5];
            break;
		case CMD_SET_FHSS:
			NV_config.item.Fhss_enable = rxBuf[5];
			break;
		case CMD_SET_RF_CHANNEL:
			NV_config.item.freq_index = rxBuf[5];
			break;
		case CMD_SET_POWER:
			NV_config.item.power = (rxBuf[5] << 8) | rxBuf[6];
			break;
		case CMD_SET_ENV_MODE:
			NV_config.item.reader_mode = rxBuf[5];
			break;
		case CMD_SET_UART_BAUDRATE:
			Set_UART0_BAUD(__UART_CLK,((rxBuf[5] << 8) | rxBuf[6]) * 100);
			Set_UART1_BAUD(__UART_CLK,((rxBuf[5] << 8) | rxBuf[6]) * 100);
			break;
		case CMD_INSERT_FHSS_CHANNEL:
		{
			uchar channelCnt = rxBuf[5];
			uchar i;
			uchar j;
			if (channelCnt == 0)
			{
				NV_config.item.freq_list.chCnt = 0;
			}
			else if (channelCnt > MAX_CHANNEL_NUM)
			{
				break;
			}
			for (i = 0; i < channelCnt; i++)
			{
				for (j = 0; j < NV_config.item.freq_list.chCnt; j++)
				{
					if (NV_config.item.freq_list.freqPoint[j] == rxBuf[6+i])
					{
						break;
					}
				}
				if (j == NV_config.item.freq_list.chCnt) // Not found a same freqPoint
				{
					NV_config.item.freq_list.freqPoint[NV_config.item.freq_list.chCnt] = rxBuf[6+i];
					NV_config.item.freq_list.chCnt++;
				}
			}
		}
			break;
//		case CMD_SET_READ_ADDR:
//			SetReaderAddr(rxBuf[4], rxBuf[5]);
//			break;
		case CMD_POWERDOWN_MODE:
			SetPowerDownMode(TRUE);
			break;
		case CMD_RESTART:
			Restart(rxBuf[5], rxBuf[6]);
			break;
// 		case CMD_IO_CONTROL:
// 			IoControl(rxBuf[5],rxBuf[6],rxBuf[7]);
// 			break;
		case CMD_SET_SLEEP_TIME:
			SetSleepTime(rxBuf[5]);
			break;

// 		case CMD_TEST_RESET:
// 			echo();
// 			ResetFW();
// 			break;
		case CMD_SAVE_NV_CONFIG:
			Save_NV_config(rxBuf[5]);
			send_success(CMD_SAVE_NV_CONFIG);
			break;
		case CMD_LOAD_NV_CONFIG:
			if (Load_NV_config() != 0)
			{
				ConfigReader();
				send_success(CMD_LOAD_NV_CONFIG);
			}
			else
			{
				send_fail(CMD_LOAD_NV_CONFIG);
			}
			break;
		case CMD_ENABLE_FW_ISP_UPDATE:
			if (EnableFWIspUpdate(rxBuf[5] == 0x00 ? FALSE : TRUE))
			{
				send_success(CMD_ENABLE_FW_ISP_UPDATE);
			}
			else
			{
				send_fail(CMD_ENABLE_FW_ISP_UPDATE);
			}
			break;

		default:
			g_readerResponseFlag = 0; // the other commands will be handled in reader chip, and has response!
			break;
	}
}

void echo()
{
    int i;
    for (i = 0; i < frameLength; i++)
    {
        Send_Byte_To_PC(rxBuf[i]);
    }
}

int32_t main()
{
    Un_Lock_Reg();                                     //中:解锁被保护的寄存器位，以便用户访问//en:Unlock protected register bits, so that user can access them     
    PWRCON = (PWRCON & (~OSC22M_EN)) | OSC22M_EN;         //中:使能外部22MHz晶振//en:Enable external 22MHz crystal
    while((CLKSTATUS & OSC22M_STB) == 0);                 //中:等22M晶振时钟稳定//en:Wait until external 22M crystal stable
    CLKSEL0 = (CLKSEL0 & (~HCLK)) | HCLK_22M;             //中:选外部22MHz晶振为系统时钟//en:Select 22M as system clock

    Lock_Reg();                                        //中:重新锁被保护的寄存器位//en:Re-lock protected register bits

    P2_PMD = (P2_PMD & 0xFFFFCFFF) | 0xFFFFDFFF; //Set P26(which connect to reader chip En pin) Dout mode 
    //reset reader chip
    DISABLE_READER();
    Delayms(1);
    ENABLE_READER();
    
    UART0_Init();
    UART1_Init();
    WatchDogInit();
    data_from_reader = 0;
    data_from_PC = 0;
    FeedWatchDog();

    UA0_IER	|= (RDA_IEN	| RLS_IEN);                    //中:使能中断//en:Enable UART0 interrupt
    NVIC_ISER |= UART0_INT;                            //中:使能NVIC UART0中断//en:Enable NVIC UART0 interrupt
    
    needDownload = TRUE;
    Download_FW();
    if (Load_NV_config() != 0)
    {
        Delayms(10);
        ConfigReader();
    }
    else
    {
    	Initialize_NV_item();
    }
    needDownload = FALSE;

    UA1_IER	|= (RDA_IEN | RLS_IEN);         //中:使能中断//en:Enable UART1 interrupt
    NVIC_ISER |= UART1_INT;                             //中:使能NVIC UART1中断//en:Enable NVIC UART1 interrupt
    
    NVIC_ISER |= PWRWU_INT;
    
    TMR0Init();  // m0 idle timer

    while(1)
    {
        FeedWatchDog();
        if (needDownload)
        {
            Delayus(5000);
            Download_FW();
            Delayms(10); // wait reader firmware go into the parse_run loop
            ConfigReader();
            needDownload = FALSE;
//          TMR0Init();
        }
        if(frameEndFlag)
	    {
            uint8_t checkSum = 0;
            uint8_t i;
            g_cmdInProgress = 1;
//          echo();
            for (i = 1; i < frameLength - 2; i++)
            {
                checkSum += rxBuf[i];
            }
            if (!(checkSum == rxBuf[frameLength-2]))
            {
                //("Frame checkSum Not Correct!");
//              send_exe_status(rxBuf[3], 0x55); this will be sent by reader
            }
            else if (rxBuf[frameLength-1] != FRAME_END)
            {
                //("Frame End Not found when data counter ends");
            }
            else if (rxBuf[3] > 0)
            {
                // Frame length too long
            }
            else
            {
                parse_run();
            }
            frameEndFlag = 0;
            TMR0Init();
	    }
        if (timeToSleepMin != 0)
        {
            if (g_idleTimeSec >= timeToSleepMin * 60)
            {
                SetPowerDownMode(FALSE);
            }
        }
        // not got any bytes from reader chip since last PC command in 2 seconds!
        // the M100/QM100 FW might run away, so send CMD_HEART_BEAT to check if the reader chip is still on
        if (g_cmdTimeConsuming >= 2)
        {
            if ((g_readerResponseFlag & 0x02) == 0)
            {
                if (frameBeginFlag == 0) // no command in progress
                {
                    frame_data_t cmdDat;
                    cmdDat.byteLen = 0;
                    g_readerResponseFlag = 0x02;
                    g_heartBeatFlag = 0;
                    while((UA1_FSR & TE_FLAG) == 0); // wait for last byte received
                    send_cmd_to_reader(CMD_HEART_BEAT, &cmdDat);
                    g_cmdInProgress = 1;
                }
            }
            // check if command executing consuming time is longer than 3 second from last CMD_HEART_BEAT
            if (g_cmdTimeConsuming >= 3)
            {
                ResetReaderChip();
                g_readerResponseFlag = 1;
                g_cmdTimeConsuming = 0;
            }
        }// the module has no command during CHECK_READER_CHIP_INTERVAL, so send heart beat packet(CMD_HEART_BEAT) to check if the reader chip is still on
        else if ((g_idleTimeSec > 0) && (g_idleTimeSec % CHECK_READER_CHIP_INTERVAL == 0))
        {
            if ((g_readerResponseFlag & 0x02) == 0)
            {
                if (frameBeginFlag == 0) // no command in progress
                {
                    frame_data_t cmdDat;
                    cmdDat.byteLen = 0;
                    g_readerResponseFlag = 0x02;
                    g_heartBeatFlag = 0;
                    while((UA1_FSR & TE_FLAG) == 0); // wait for last byte received
                    send_cmd_to_reader(CMD_HEART_BEAT, &cmdDat);
                    g_cmdInProgress = 1;
                }
            }
        }
        else
        {
            g_readerResponseFlag = 1;
        }
    }
}	
