/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright(c) 2011 Nuvoton Technology Corp. All rights reserved.                                         */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdint.h>
#include "M051.h"
#include "Register_Bit.h"

#if defined ( __CC_ARM   )
#if (__ARMCC_VERSION < 400000)
#else
/* Insist on keeping widthprec, to avoid X propagation by benign code in C-lib */
#pragma import _printf_widthprec
#endif
#endif

//#define DEBUG_ENABLE_SEMIHOST   /* To enable semihosted. !!!The SEMIHOSTED of startup_MINI51.s must be {TRUE} */
/*---------------------------------------------------------------------------------------------------------*/
/* Macro Definition                                                                                        */
/*---------------------------------------------------------------------------------------------------------*/
/* Using UART0 or UART1 */  
												   
#define DEBUG_PORT  1     	/* 0:UART0   1:UART1 */
/*---------------------------------------------------------------------------------------------------------*/
/* Global variables                                                                                        */
/*---------------------------------------------------------------------------------------------------------*/
#if !(defined(__ICCARM__) && (__VER__ >= 6010000))
struct __FILE { int handle; /* Add whatever you need here */ };
#endif
FILE __stdout;
FILE __stdin;

#if defined(DEBUG_ENABLE_SEMIHOST)
/* The static buffer is used to speed up the semihost */
static uint8_t g_buf[16];
static uint8_t g_buf_len = 0;

/* The function to process semihosted command */
extern int32_t SH_DoCommand(int32_t n32In_R0, int32_t n32In_R1, int32_t *pn32Out_R0);
#endif

/*en:************************************************************************************
  Parameter:	        	ch: char to be sent.                                    
  Description:          Send a char to UART.                                   
****************************************************************************************/
/*中:************************************************************************************
 参数 :  ch: 要发送到字符
 描述:   发送一个字符到UART
****************************************************************************************/
void SendChar_ToUART(int32_t ch)
{
    if(DEBUG_PORT == 0)
    {
        while((UA0_FSR&TX_FULL) != 0);                       //中:发送FIFO满时等待//en:Wait until UART0 transmit FIFO is not full
        UA0_THR = ch;                                        //中:通过UART0发送一个字符//en:Transmit a char via UART0

        if(ch == '\n')
        {
            while((UA0_FSR&TX_FULL) != 0);                   //中:发送FIFO满时等待//en:Wait until UART0 transmit FIFO is not full
            UA0_THR = '\r';                                  //中:通过UART0发送一个回车符//en:Transmit a '\r' char via UART0
        }
    }
    else
    {
        while((UA1_FSR&TX_FULL) != 0);                       //中:发送FIFO满时等待//en:Wait until UART1 transmit FIFO is not full
        UA1_THR = ch;                                        //中:通过UART1发送一个字符//en:Transmit a char via UART1

        if(ch == '\n')
        {
            while((UA1_FSR&TX_FULL) != 0);                   //中:发送FIFO满时等待//en:Wait until UART1 transmit FIFO is not full
            UA1_THR = '\r';                                  //中:通过UART1发送一个回车符//en:Transmit a '\r' char via UART1
        }
    }
}

/*en:************************************************************************************
  Parameter:	        	ch: char to be sent.                                    
  Description:          Send a char via UART to display on PC hyper-terminal in normal 
                        mode or keil I/O window in Semihost mode when debuging.                                   
****************************************************************************************/
/*中:************************************************************************************
 参数 :  ch: 要发送的字符
 描述:   通过UART发送一个字符到PC超级终端或者Keil的输入/输出窗口(在调试状态半主机模式下)
****************************************************************************************/
void SendChar(int32_t ch)
{
#if defined(DEBUG_ENABLE_SEMIHOST)
    g_buf[g_buf_len++] = ch;
    g_buf[g_buf_len] = '\0';
    if(g_buf_len + 1 >= sizeof(g_buf) || ch == '\n' || ch == '\0')
    {
        if(SH_DoCommand(0x04, (int32_t)g_buf, NULL) != 0)    //中:发送字符//en:Send a char
        {
            g_buf_len = 0;
            return;
        }
        else
        {
            int32_t i;

            for(i=0;i<g_buf_len;i++)
                SendChar_ToUART(g_buf[i]);
            g_buf_len = 0;
        }
    }
#else
    SendChar_ToUART(ch);                                     //中:发送字符//en:Send a char
#endif
}

/*en:************************************************************************************
  Return:	        	    A char.                                    
  Description:          Get a char via UART.                                   
****************************************************************************************/
/*中:************************************************************************************
 返回值: 一个字符
 描述:   通过UART获取一个字符
****************************************************************************************/
char GetChar(void)
{
#ifdef DEBUG_ENABLE_SEMIHOST
    int32_t nRet;
    while(SH_DoCommand(0x7, 0, &nRet) != 0)                  //中:获取字符//en:Get a char
    {                
        if(nRet != 0)
        {
            return (uint8_t)nRet;
        }
    }
#endif

    while(1)
    {
        if(DEBUG_PORT == 0)
        {
            if((UA0_FSR&RX_EMPTY) == 0)                      //中:等字符//en:Wait until an avaliable char present in RX FIFO
                return ((uint8_t)UA0_RBR);                   //中:返回接收到的字符//en:Return received char
        }
        else
        {
            if((UA1_FSR&RX_EMPTY) == 0)                      //中:等字符//en:Wait until an avaliable char present in RX FIFO
                return ((uint8_t)UA1_RBR);                   //中:返回接收到的字符//en:Return received char
        }
    }
}

/*---------------------------------------------------------------------------------------------------------*/
/* C library retargetting                                                                                  */
/*---------------------------------------------------------------------------------------------------------*/

void _ttywrch(int32_t ch)
{
    SendChar(ch);
    return;
}

int32_t fputc(int32_t ch, FILE *f)
{
    SendChar(ch);
    return ch;
}

int32_t fgetc(FILE *f) 
{
    return (GetChar());
}

int32_t ferror(FILE *f) 
{
    return EOF;
}
