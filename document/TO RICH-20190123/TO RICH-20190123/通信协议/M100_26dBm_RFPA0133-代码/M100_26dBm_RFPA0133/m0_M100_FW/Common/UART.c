/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright(c) 2011 Nuvoton Technology Corp. All rights reserved.                                         */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdint.h>
#include "M051.h"
#include "Register_Bit.h"
#include "Uart.h"

volatile uint8_t data_from_reader;
volatile uint8_t data_from_PC;

frame_data_t resDat;

void Delayus(uint32_t unCnt)
{
    SYST_RVR = unCnt*22; //unCnt*12;
    SYST_CVR = 0;
    SYST_CSR |=1UL<<0;

    while((SYST_CSR & 1UL<<16)==0);
}

void Delayms(uint32_t unCnt)
{
#if 0
	 static uint8_t b=1;

	 if(b)
	 {
	    b=0;
	    TMR1_Clock_EN;
		TMR1ClkSource_ex12MHz;
	    TCSR1  = 0x00000001;    //Pre-Scaler
	    
	    setTMR1_PERIOD;
	    setTMR1_IE;             //Timer0 interrupt enable
	    setTMR1_CRST;           //Reset the timer/counter0, after set, this bit will be clear by H/W	 
	 }

	  TCMPR1 = 22118;           //Fosc=22.1184MHz, so 22118400/22118=1000Hz=1ms
	  setTMR1_CEN;              //启动 TMR0

	  while (unCnt != 1)
	  {
	      while ((TISR1&TMR_TIF) != TMR_TIF); //检查 TIF0
	      TISR1 |= TMR_TIF;     //清零 TIF0
	      unCnt --;
	  }

	  clrTMR1_CEN;              //停止TMR0
#else

      SYST_RVR = unCnt*221184;
	  SYST_CVR = 0;
	  SYST_CSR |=1UL<<0;
	  
	  while((SYST_CSR & 1UL<<16)==0); 

#endif
}


void Set_UART0_BAUD(uint32_t unFosc,uint32_t unBaud)
{
    //wait UART is free
    while((UA0_FSR & TE_FLAG) == 0);
    //模式2波特率=UART_CLK/(UA_BAUD+2)//en:BaudRate==UART_CLK/(UA_BAUD+2) in mode 2
    UA0_BAUD = ((UA0_BAUD & 0xFFFF0000) | ((unFosc / unBaud) -2));
}

/*en:************************************************************************************
Description:      Initialize UART.                                
****************************************************************************************/
/*中:************************************************************************************
 描述:  初始化UART0
****************************************************************************************/
void UART0_Init(void)
{ 
    P3_MFP = P3_MFP & (~(P31_TXD0 | P30_RXD0)) | (TXD0 | RXD0); //配置P3.1和P3.0为UART功能//en:Set P3.1 and P3.0 to UART0 function

    IPRSTC2 |= UART0_RST;                            //复位UART模块//en:Reset UART0 module
    IPRSTC2 &= ~UART0_RST;                           //UART模块从复位状态恢复到正常工作状态//en:Resume UART0 to normal mode

    APBCLK |= UART0_CLKEN;                           //使能UART时钟//en:Enable UART0 clock
//    CLKSEL1 = CLKSEL1 & (~UART_CLK) | UART_12M;      //选择外部12M作为UART时钟源//en:Select external 12M as UART0 clock source
    CLKSEL1 = (CLKSEL1 & (~UART_CLK)) | UART_22M;      //选择内部22M作为UART时钟源//en:Select internal 22M as UART0 clock source
    CLKDIV &= ~(15<<8);                              //设置UART时钟除频值为0//en:UART clock no division

    UA0_FCR |= TX_RST;                               //复位发送FIFO//en:Reset transmit FIFO
    UA0_FCR |= RX_RST;                               //复位接收FIFO//en:Reset receive FIFO

    UA0_LCR &= ~PBE;                                 //校验位禁止//en:None parity
    UA0_LCR = (UA0_LCR & (~WLS)) | WL_8BIT;          //数据宽度为8位//en:Data width 8 bits
    UA0_LCR &= NSB_ONE;                              //1位停止位//en:1 bit stop

    UA0_BAUD |= DIV_X_EN;                            //模式2:DIV_X_EN = 1//en:Mode2:DIV_X_EN = 1
    UA0_BAUD |= DIV_X_ONE;                           //模式2:DIV_X_ONE =1//en:Mode2:DIV_X_ONE = 1
    
    Set_UART0_BAUD(__UART_CLK,9600);
}

void Set_UART1_BAUD(uint32_t unFosc,uint32_t unBaud)
{
    //wait UART is free
    while((UA1_FSR & TE_FLAG) == 0);
    //模式2波特率=UART_CLK/(UA_BAUD+2)//en:BaudRate==UART_CLK/(UA_BAUD+2) in mode 2
    UA1_BAUD = (UA0_BAUD & 0xFFFF0000) | ((unFosc / unBaud) -2);
}
void UART1_Init()
{
/*---------------------------------------------------------------------------------------------------------*/
/* Init UART                                                                                               */
/*---------------------------------------------------------------------------------------------------------*/

    /* Set P1 multi-function pins for UART1 RXD and TXD  */
    P1_MFP = P1_MFP & (~(P13_AIN3_TXD1 | P12_AIN2_RXD1)) | (TXD1 | RXD1); //配置P1.3和P1.2为UART功能
    
    IPRSTC2 |= UART1_RST;
    IPRSTC2 &= ~UART1_RST;
    
    APBCLK |= UART1_CLKEN;
    CLKSEL1 = (CLKSEL1 & (~UART_CLK)) | UART_22M;
    CLKDIV &= ~(15<<8);
    
    UA1_FCR |= TX_RST;
    UA1_FCR |= RX_RST;
    
    UA1_LCR &= ~PBE;
    UA1_LCR = (UA1_LCR & (~WLS)) | WL_8BIT;
    UA1_LCR &= NSB_ONE;
    
    UA1_BAUD |= (DIV_X_EN | DIV_X_ONE);
    
    Set_UART1_BAUD(__UART_CLK, COMMUNICATION_BAUDRATE); //波特率设置
}

/*en:************************************************************************************
  Parameter:	        c: char to be sent.                                    
  Description:          Send a char to PC.                                   
****************************************************************************************/
/*中:************************************************************************************
 参数 :  c: 要发送到字符
 描述:   发送一个字符到PC
****************************************************************************************/
void Send_Data_Uart0 (uint8_t c)
{
    while((UA0_FSR&TX_FULL) != 0);                   //发送FIFO满时等待//en:Wait until UART transmit FIFO is not full
    UA0_THR = (uint8_t) c;                           //通过UART0发送一个字符//en:Transmit a char via UART0
}

/*en:************************************************************************************
  Parameter:	        c: char to be sent.                                    
  Description:          Send a char to UART1.                                   
****************************************************************************************/
/*中:************************************************************************************
 参数 :  c: 要发送到字符
 描述:   发送一个字符到PC
****************************************************************************************/
void Send_Data_Uart1 (uint8_t c)
{
    while((UA1_FSR&TX_FULL) != 0);                   //发送FIFO满时等待//en:Wait until UART transmit FIFO is not full
    UA1_THR = (uint8_t) c;                           //通过UART1发送一个字符//en:Transmit a char via UART1
}

void Send_Byte_To_PC (uint8_t c)
{
#ifdef __UART1_TO_PC___
    Send_Data_Uart1(c);
#else
    Send_Data_Uart0(c);
#endif
}

void Send_Byte_To_MODULE (uint8_t c)
{
#ifdef __UART1_TO_PC___
    Send_Data_Uart0(c);
#else
    Send_Data_Uart1(c);
#endif
}

/*en:************************************************************************************
  Return:	        	    A char.                                    
  Description:          Get a char from PC.                                   
****************************************************************************************/
/*中:************************************************************************************
 返回值: 一个字符
 描述:   从PC获取一个字符
****************************************************************************************/
uint8_t Receive_Data_From_Uart0(void)
{
    while((UA0_FSR&RX_EMPTY) != 0);                  //等字符//en:Wait until an avaliable char present in RX FIFO
    return ((uint8_t)UA0_RBR);                       //返回接收到的字符//en:Return received char
}

void send_response(uint8_t cmd_type, frame_data_t * resDat)
{
	uint8_t i;
	uint8_t checksum;;
	Send_Byte_To_PC(FRAME_HEAD);
	Send_Byte_To_PC(FRAME_RES);
	Send_Byte_To_PC(cmd_type);
	Send_Byte_To_PC(0x00);
	Send_Byte_To_PC(resDat->byteLen);
	checksum = FRAME_RES + cmd_type + resDat->byteLen;
	for (i = 0; i < resDat->byteLen; i++)
	{
		checksum += resDat->dat[i];
		Send_Byte_To_PC(resDat->dat[i]);
	}
	Send_Byte_To_PC(checksum);
	Send_Byte_To_PC(FRAME_END);
}

void send_fail(uint8_t fail_type)
{
	uint8_t checkSum = 0x00;
	Send_Byte_To_PC(FRAME_HEAD);
	Send_Byte_To_PC(FRAME_RES);
	Send_Byte_To_PC(CMD_FAIL);
	Send_Byte_To_PC(0x00);
	Send_Byte_To_PC(0x01);
	Send_Byte_To_PC(fail_type);
	checkSum = fail_type + FRAME_RES + CMD_FAIL + 0x01 ;	 //FRAME_RES + CMD_FAIL + 0x01 + fail_type
	Send_Byte_To_PC(checkSum);
	Send_Byte_To_PC(FRAME_END);
}

void send_success(uint8_t cmd_type)
{
	uint8_t checkSum = 0x00;
	Send_Byte_To_PC(FRAME_HEAD);
	Send_Byte_To_PC(FRAME_RES);
	Send_Byte_To_PC(cmd_type); //cmd which executes successes
	Send_Byte_To_PC(0x00);
	Send_Byte_To_PC(0x01);
	Send_Byte_To_PC(0x00);
	checkSum = FRAME_RES + cmd_type + 0x01;
	Send_Byte_To_PC(checkSum);
	Send_Byte_To_PC(FRAME_END);
}

void send_cmd_to_reader(uint8_t cmd_type, frame_data_t * cmdDat)
{
    uint8_t i;
	uint8_t checksum;
	Send_Byte_To_MODULE(FRAME_HEAD);
	Send_Byte_To_MODULE(FRAME_CMD);
	Send_Byte_To_MODULE(cmd_type);
	Send_Byte_To_MODULE(0x00);
	Send_Byte_To_MODULE(cmdDat->byteLen);
	checksum = FRAME_CMD + cmd_type + cmdDat->byteLen;
	for (i = 0; i < cmdDat->byteLen; i++)
	{
		checksum += cmdDat->dat[i];
		Send_Byte_To_MODULE(cmdDat->dat[i]);
	}
	Send_Byte_To_MODULE(checksum);
	Send_Byte_To_MODULE(FRAME_END);
}
