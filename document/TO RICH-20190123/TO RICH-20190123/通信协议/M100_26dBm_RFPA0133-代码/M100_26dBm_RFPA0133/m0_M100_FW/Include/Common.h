/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright(c) 2010 Nuvoton Technology Corp. All rights reserved.                                         */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

#ifndef   __COMMON_H__
#define   __COMMON_H__

#include <stdint.h>
#include <stdio.h>
#include "M051.h"
#include "Register_Bit.h"
#include "Uart.h"

#ifndef LITTLE_ENDPOINT
#define LITTLE_ENDPOINT
#endif

#ifdef  LITTLE_ENDPOINT
#define SWAP16(x)   (x)
#else
#define SWAP16(x)  (((UINT8)(x)<<8)|(UINT8)((x)>>8))
#endif

#define LSB(x)     ((UINT8)(x))
#define MSB(x)     ((UINT8)(((UINT16)(x))>>8)) 

#define __WFI				  __wfi

#define PROTECT_REG(__CODE)		  {Un_Lock_Reg();__CODE;Lock_Reg();}

void Un_Lock_Reg(void);
void Lock_Reg(void);
void PLL_Enable(void);
void NSR_Enable(void);
void NSR_Disable(void);

#endif  //__COMMON_H__
