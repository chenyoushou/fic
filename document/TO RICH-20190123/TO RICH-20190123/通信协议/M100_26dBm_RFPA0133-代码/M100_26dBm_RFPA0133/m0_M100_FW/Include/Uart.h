/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright(c) 2010 Nuvoton Technology Corp. All rights reserved.                                         */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

#ifndef __UART_H__
#define __UART_H__

#define __UART_CLK  (22118400)

#define COMMUNICATION_BAUDRATE  (115200)

#define TX_ONE_BYTE_TIME_us     (1000000 * 10 / COMMUNICATION_BAUDRATE)

#define __UART1_TO_PC___

//UART1 connected to PC
//UART0 connected to RFID module

#define RXBUFSIZE 64
#define RX_BUF_SIZE 128

#define  FRAME_HEAD      (0xBB)
#define  FRAME_END       (0x7E)

#define  MY_INIT_ADDR    (0xAA)
#define  PUBLIC_ADDR     (0xFF)
#define  BROADCAST_ADDR  (0xFE)

#define  HEART_BEAT_RESPONSE      ('H')

#define REGION_CHN2  (1)
#define REGION_CHN1  (4)
#define REGION_US    (2)
#define REGION_EUR   (3)
#define REGION_JAPAN (5)
#define REGION_KOREA (6)

#define FHSS_ON  0xFF
#define FHSS_OFF 0x00

typedef struct {
	uint8_t byteLen;
	uint8_t dat[127];
} frame_data_t;

enum frame_type {
	FRAME_CMD = 0x00,
	FRAME_RES = 0x01,
	FRAME_INFO = 0x02,
	FRAME_ERROR = 0xFF
};

enum cmd_code {
	CMD_HELLO 			= 0x01,
	CMD_HEART_BEAT      = 0x02,
	CMD_GET_MODULE_INFO	= 0x03,
	CMD_SINGLE_ID 		= 0x22,
	CMD_MULTI_ID 		= 0x27,
	CMD_STOP_MULTI 		= 0x28,
	CMD_READ_DATA 		= 0x39,
	CMD_WRITE_DATA 		= 0x49,
	CMD_LOCK_UNLOCK 	= 0x82,
	CMD_KILL 			= 0x65,
	CMD_SET_REGION      = 0x07,
	CMD_INSERT_FHSS_CHANNEL = 0xA9,
	CMD_GET_RF_CHANNEL  = 0xAA,
	CMD_SET_RF_CHANNEL  = 0xAB,
	CMD_SET_CHN2_CHANNEL= 0xAF,
	CMD_SET_US_CHANNEL 	= 0xAC,		  // For RFCONN Conference
	CMD_OPEN_PA		 	= 0xAE,		  // For RFCONN Conference
	CMD_SET_FHSS        = 0xAD,
	CMD_SET_POWER 	    = 0xB6,
	CMD_GET_POWER 	    = 0xB7,
	CMD_GET_SELECT_PARA	= 0x0B,
	CMD_SET_SELECT_PARA = 0x0C,
	CMD_GET_QUERY_PARA 	= 0x0D,
	CMD_SET_QUERY_PARA 	= 0x0E,
	CMD_SET_CW 			= 0xB0,
	CMD_SET_BLF 		= 0xBF,
	CMD_FAIL 			= 0xFF,
	CMD_SUCCESS 		= 0x00,
	CMD_SET_SFR 		= 0xFE,
	CMD_READ_SFR 		= 0xFD,
	CMD_INIT_SFR 		= 0xEC,
	CMD_CAL_MX   		= 0xEA,
	CMD_CAL_LPF  		= 0xED,
	CMD_READ_MEM		= 0xFB,
	CMD_SET_INV_MODE    = 0x12,
	CMD_SET_UART_BAUDRATE = 0x11,

	CMD_SCAN_JAMMER     = 0xF2,
	CMD_SCAN_RSSI       = 0xF3,
	CMD_AUTO_ADJUST_CH  = 0xF4,

	CMD_SET_MODEM_PARA  = 0xF0,
	CMD_READ_MODEM_PARA = 0xF1,
	CMD_SET_ENV_MODE    = 0xF5,
	CMD_TEST_RESET      = 0x55,

	CMD_POWERDOWN_MODE  = 0x17,
	CMD_SET_SLEEP_TIME  = 0x1D,
	CMD_IO_CONTROL      = 0x1A,
	CMD_RESTART         = 0x19,
	CMD_LOAD_NV_CONFIG  = 0x0A,
	CMD_SAVE_NV_CONFIG  = 0x09,
	CMD_ENABLE_FW_ISP_UPDATE = 0x1F,
	
	CMD_SET_READ_ADDR    = 0x14
};

enum fail_code {
//	FAIL_READ_MULTI_TAG = 0x0B,
	FAIL_INVALID_PARA = 0x0E,
	FAIL_INVENTORY_TAG_TIMEOUT = 0x15,
	FAIL_INVALID_CMD = 0x17,

	FAIL_FHSS_FAIL = 0x20,

	FAIL_ACCESS_PWD_ERROR = 0x16,

	FAIL_READ_MEMORY_NO_TAG = 0x09,
	FAIL_READ_ERROR_CODE_BASE = 0xA0,

	FAIL_WRITE_MEMORY_NO_TAG = 0x10,
	FAIL_WRITE_ERROR_CODE_BASE = 0xB0,

	FAIL_LOCK_NO_TAG = 0x13,
	FAIL_LOCK_ERROR_CODE_BASE = 0xC0,

	FAIL_KILL_NO_TAG = 0x12,
	FAIL_KILL_ERROR_CODE_BASE = 0xD0,
    
    FAIL_WATCHDOG_OVERFLOW = 0x05
};

enum module_info_code {
	MODULE_HARDWARE_VERSION = 0x00,
	MODULE_SOFTWARE_VERSION = 0x01,
	MODULE_MANUFACTURE_INFO = 0x02
};

enum environment_mode
{
	MODE_DENSE_READER = 0x01,
	MODE_HIGH_SENSITIVITY = 0x00
};

enum inventory_mode
{
	INVENTORY_MODE0 = 0x00, // User has set Select parameter
	INVENTORY_MODE1 = 0x01,	// fast tracking
	INVENTORY_MODE2 = 0x02  // multi-tag
};

extern volatile uint8_t data_from_reader;
extern volatile uint8_t data_from_PC;

extern frame_data_t resDat;

void Delayus(uint32_t unCnt);
void Delayms(uint32_t unCnt);

void Set_UART0_BAUD(uint32_t unFosc,uint32_t unBaud);
void UART0_Init(void);
void Set_UART1_BAUD(uint32_t unFosc,uint32_t unBaud);
void UART1_Init(void);
void Send_Data_Uart0 (uint8_t c);
void Send_Data_Uart1 (uint8_t c);
void Send_Byte_To_PC (uint8_t c);
void Send_Byte_To_MODULE (uint8_t c);
uint8_t Receive_Data_From_Uart0(void);
void send_response(uint8_t cmd_type, frame_data_t * resDat);
void send_fail(uint8_t fail_type);
void send_success(uint8_t cmd_type);
void send_cmd_to_reader(uint8_t cmd_type, frame_data_t * cmdDat);

#endif
