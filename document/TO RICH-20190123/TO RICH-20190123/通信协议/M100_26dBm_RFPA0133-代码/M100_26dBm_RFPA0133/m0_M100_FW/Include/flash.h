#ifndef   __FLASH_H__
#define   __FLASH_H__

#include "common.h"
#include "typedef.h"

#define PAGE_SIZE		         512
#define DATAFLASH_SIZE           0x00001000
#define DATAFLASH_START_ADDRESS  0x0001F000
#define CONFIG_START_ADDR        0x00300000
#define CONFIG_BOOT_FROM_APROM   0x00000080

/****************************************
*函数名称:DataFlashErase
*输  入:unPage  页地址
*输  出:无
*功  能:数据区擦除
******************************************/
VOID DataFlashErase(UINT32 unPage);

/****************************************
*函数名称:DataFlashWrite
*输  入:pucBuf  写数据缓冲区
unSize  写数据大小
*输  出:无
*功  能:数据区写
******************************************/
VOID DataFlashWrite(UINT8 *pucBuf,UINT32 unSize); //unSize 要为 4 的倍数

/****************************************
*函数名称:DataFlashRead
*输  入:pucBuf  读数据缓冲区
unSize  读数据大小
*输  出:无
*功  能:数据区读
******************************************/
VOID DataFlashRead(UINT8 *pucBuf,UINT32 unSize);

/****************************************
*函数名称:writetoFlash
*输  入:offset  写入数据地址
				data    数据
*输  出:无
*功  能:数据区写
******************************************/
void writetoFlash(uint32_t offset,uint32_t date);

/****************************************
*函数名称:readfromFlash
*输  入:offset 数据地址
*输  出:数据，32bit
*功  能:数据区读
******************************************/
uint32_t readfromFlash(uint32_t offset);

/****************************************
*函数名称:ConfigEnable
*输    入:无
*输    出:无
*功    能:Config使能
******************************************/
VOID ConfigEnable(VOID);

/****************************************
*函数名称:Config0Read
*输    入:无
*输    出:无
*功    能:Config0区 读
******************************************/
UINT32 Config0Read(VOID);

/****************************************
*函数名称:ConfigErase
*输    入:无
*输    出:无
*功    能:Config区 擦除
******************************************/
VOID ConfigErase(VOID);

/****************************************
*函数名称:Config0Write
*输    入:无
*输    出:无
*功    能:Config0区 写
******************************************/
VOID Config0Write(UINT32 unData);

/****************************************
*函数名称:EnableFWIspUpdate
*输    入:isEnable, TRUE:使能固件通过ISP更新，FALSE:不使能，m0上电从APROM启动
*输    出:TRUE,使能成功; FALSE:失败
*功    能:使能固件通过ISP方式更新，即m0上电后从LDROM启动，LDROM中应存有ISP固件
******************************************/
BOOL EnableFWIspUpdate(BOOL isEnable);

#endif
