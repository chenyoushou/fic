#ifndef   __WATCHDOG_H__
#define   __WATCHDOG_H__

#include "common.h"
#include "typedef.h"

/****************************************
*函数名称:WatchDogInit
*输    入:无
*输    出:无
*功    能:看门狗初始化
******************************************/
VOID WatchDogInit(VOID);

/****************************************
*函数名称:FeedWatchDog
*输    入:无
*输    出:无
*功    能:喂狗，清除WatchDog寄存器
******************************************/
VOID FeedWatchDog(VOID);

VOID DisableWatchDog(VOID);
VOID EnableWatchDog(VOID);

#endif
