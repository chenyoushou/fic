
#ifndef   __RFID_FIRMWARE_H__
#define   __RFID_FIRMWARE_H__

#include "common.h"
#include "typedef.h"

#define ENABLE_READER()    {P26_DOUT = 1;}
#define DISABLE_READER()   {P26_DOUT = 0;}

#define NV_CONFIG_DWORD_SIZE    (100)

#define MAX_CHANNEL_NUM  (52)
#define NV_ENABLE        (1)
#define NV_DISABLE       (0)

typedef unsigned char       byte;
typedef unsigned char       uchar;
typedef unsigned int        word;
typedef unsigned long int   dword;

typedef struct
{
	uchar chCnt;
	uchar freqPoint[MAX_CHANNEL_NUM];
} freq_list_t;

typedef struct
{
	dword NV_enable;
	byte sleepTime;
	byte reader_mode;
	byte Fhss_enable;
	byte region;
	freq_list_t freq_list;
	byte freq_index;
	UINT16 power;
    dword checksum;
} NV_config_type;

typedef union
{
	dword dword[NV_CONFIG_DWORD_SIZE];
	NV_config_type item;
} data_flash_config_t;

extern data_flash_config_t NV_config;

extern volatile BOOL needDownload;
extern volatile uint32_t timeToSleepMin;

uint32_t Download_FW(void);
void ResetReaderChip(void);
void ConfigReader(void);
void Initialize_NV_item(void);
uint32_t Load_NV_config(void);
BOOL Save_NV_config(uint32_t NV_enable);

#endif
