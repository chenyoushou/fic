#ifndef __TIMER_H__
#define __TIMER_H__
#include "Common.h"

extern volatile uint32_t g_idleTimeSec;
extern volatile uint32_t g_cmdTimeConsuming;
extern volatile uint8_t g_cmdInProgress;

void TMR0Init(void);

#endif
