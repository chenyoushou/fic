#include "Common.h"
#include "typedef.h"
#include "Uart.h"

/********注意: WTCR所有位都写保护(包括清除计数器位),
************需要使用PROTECT_REG宏操作*********/

/****************************************
*函数名称:WatchDogInit
*输    入:无
*输    出:无
*功    能:看门狗初始化
******************************************/
VOID WatchDogInit(VOID)
{
	PROTECT_REG
	(
	    /* 使能看门狗时钟 */
		APBCLK |= WDT_CLKEN;

		/* 设置看门狗时钟源为10K */
		CLKSEL1 = (CLKSEL1 & (~WDT_CLK)) | WDT_10K;

		/* 使能看门狗定时器复位功能 */
		WTCR |= WTRE;

		/* 设置看门狗超时间隔为1740.8ms */
		WTCR &= ~WTIS;
		WTCR |= TO_2T14_CK;	//(2^14+1024)*(1000000/10000)=17408*100=1740800us=1.7408s

		/* 使能看门狗中断 */
		WTCR |= WTIE;
		NVIC_ISER |= WDT_INT;

		/* 使能看门狗 */
		WTCR |= WTE;

		/* 复位看门狗计数值 */
		WTCR |= CLRWTR;
	)
}

/****************************************
*函数名称:FeedWatchDog
*输    入:无
*输    出:无
*功    能:喂狗，清除WatchDog寄存器
******************************************/
VOID FeedWatchDog(VOID)
{
    PROTECT_REG
	(
        WTCR |= CLRWTR;         //喂狗操作
    )
}

/****************************************
*函数名称:DisableWatchDog
*输    入:无
*输    出:无
*功    能:禁用WatchDog
******************************************/
VOID DisableWatchDog(VOID)
{
    PROTECT_REG
	(
        WTCR &= ~WTE;
//        WTCR &= ~WTIE;
//        NVIC_ISER &= ~WDT_INT;
    )
}

/****************************************
*函数名称:EnableWatchDog
*输    入:无
*输    出:无
*功    能:使能WatchDog
******************************************/
VOID EnableWatchDog(VOID)
{
    PROTECT_REG
	(
//        WTCR |= WTIE;
//        NVIC_ISER |= WDT_INT;
        WTCR |= WTE;
    )
}

/****************************************
*函数名称:WDT_IRQHandler
*输    入:无
*输    出:无
*功    能:看门狗中断服务函数
******************************************/
VOID WDT_IRQHandler(VOID) 
{ 
    send_fail(FAIL_WATCHDOG_OVERFLOW); //打印复位信息

	PROTECT_REG
	(
		WTCR |= WTWKF;
		WTCR |= WTIF;	
	)
}

